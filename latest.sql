PGDMP     1    7                v            deqa8vtb4ubj7o     10.2 (Ubuntu 10.2-1.pgdg14.04+1)    10.2 �    \           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            ]           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            ^           1262    5937065    deqa8vtb4ubj7o    DATABASE     �   CREATE DATABASE "deqa8vtb4ubj7o" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8';
     DROP DATABASE "deqa8vtb4ubj7o";
             jyvipeultswzys    false                        2615    2200    public    SCHEMA        CREATE SCHEMA "public";
    DROP SCHEMA "public";
             jyvipeultswzys    false            _           0    0    SCHEMA "public"    COMMENT     8   COMMENT ON SCHEMA "public" IS 'standard public schema';
                  jyvipeultswzys    false    7                        3079    13809    plpgsql 	   EXTENSION     C   CREATE EXTENSION IF NOT EXISTS "plpgsql" WITH SCHEMA "pg_catalog";
    DROP EXTENSION "plpgsql";
                  false            `           0    0    EXTENSION "plpgsql"    COMMENT     B   COMMENT ON EXTENSION "plpgsql" IS 'PL/pgSQL procedural language';
                       false    1            �            1259    8116276    about_amies    TABLE     �  CREATE TABLE "about_amies" (
    "id" bigint NOT NULL,
    "meet_amy" "text",
    "background_img_file_name" character varying,
    "background_img_content_type" character varying,
    "background_img_file_size" integer,
    "background_img_updated_at" timestamp without time zone,
    "moabs_experience" "text",
    "bottom_ad_one_file_name" character varying,
    "bottom_ad_one_content_type" character varying,
    "bottom_ad_one_file_size" integer,
    "bottom_ad_one_updated_at" timestamp without time zone,
    "bottom_ad_two_file_name" character varying,
    "bottom_ad_two_content_type" character varying,
    "bottom_ad_two_file_size" integer,
    "bottom_ad_two_updated_at" timestamp without time zone,
    "bottom_ad_three_file_name" character varying,
    "bottom_ad_three_content_type" character varying,
    "bottom_ad_three_file_size" integer,
    "bottom_ad_three_updated_at" timestamp without time zone,
    "meta_title" character varying,
    "meta_description" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "bottom_ad_one_link" character varying,
    "bottom_ad_two_link" character varying,
    "bottom_ad_three_link" character varying
);
 #   DROP TABLE "public"."about_amies";
       public         jyvipeultswzys    false    7            �            1259    8116274    about_amies_id_seq    SEQUENCE     v   CREATE SEQUENCE "about_amies_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE "public"."about_amies_id_seq";
       public       jyvipeultswzys    false    7    224            a           0    0    about_amies_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE "about_amies_id_seq" OWNED BY "about_amies"."id";
            public       jyvipeultswzys    false    223            �            1259    8116478    about_moabs    TABLE     �  CREATE TABLE "about_moabs" (
    "id" bigint NOT NULL,
    "main_image_file_name" character varying,
    "main_image_content_type" character varying,
    "main_image_file_size" integer,
    "main_image_updated_at" timestamp without time zone,
    "description" "text",
    "meta_title" character varying,
    "meta_description" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 #   DROP TABLE "public"."about_moabs";
       public         jyvipeultswzys    false    7            �            1259    8116476    about_moabs_id_seq    SEQUENCE     v   CREATE SEQUENCE "about_moabs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE "public"."about_moabs_id_seq";
       public       jyvipeultswzys    false    226    7            b           0    0    about_moabs_id_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE "about_moabs_id_seq" OWNED BY "about_moabs"."id";
            public       jyvipeultswzys    false    225            �            1259    5942578    ar_internal_metadata    TABLE     �   CREATE TABLE "ar_internal_metadata" (
    "key" character varying NOT NULL,
    "value" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 ,   DROP TABLE "public"."ar_internal_metadata";
       public         jyvipeultswzys    false    7            �            1259    8127610    blog_indices    TABLE     d  CREATE TABLE "blog_indices" (
    "id" bigint NOT NULL,
    "bottom_ad_one_file_name" character varying,
    "bottom_ad_one_content_type" character varying,
    "bottom_ad_one_file_size" integer,
    "bottom_ad_one_updated_at" timestamp without time zone,
    "bottom_ad_two_file_name" character varying,
    "bottom_ad_two_content_type" character varying,
    "bottom_ad_two_file_size" integer,
    "bottom_ad_two_updated_at" timestamp without time zone,
    "bottom_ad_three_file_name" character varying,
    "bottom_ad_three_content_type" character varying,
    "bottom_ad_three_file_size" integer,
    "bottom_ad_three_updated_at" timestamp without time zone,
    "side_ad_file_name" character varying,
    "side_ad_content_type" character varying,
    "side_ad_file_size" integer,
    "side_ad_updated_at" timestamp without time zone,
    "side_ad_link" character varying,
    "bottom_ad_one_link" character varying,
    "bottom_ad_two_link" character varying,
    "bottom_ad_three_link" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 $   DROP TABLE "public"."blog_indices";
       public         jyvipeultswzys    false    7            �            1259    8127608    blog_indices_id_seq    SEQUENCE     w   CREATE SEQUENCE "blog_indices_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE "public"."blog_indices_id_seq";
       public       jyvipeultswzys    false    7    232            c           0    0    blog_indices_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE "blog_indices_id_seq" OWNED BY "blog_indices"."id";
            public       jyvipeultswzys    false    231            �            1259    8060680    blogs    TABLE     )  CREATE TABLE "blogs" (
    "id" bigint NOT NULL,
    "title" character varying,
    "content" "text",
    "content_index" "text",
    "title_for_slug" character varying,
    "pubdate" timestamp without time zone,
    "meta_description" character varying,
    "meta_keywords" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "category_id" integer,
    "side_ad_link" character varying,
    "side_ad_file_name" character varying,
    "side_ad_content_type" character varying,
    "side_ad_file_size" integer,
    "side_ad_updated_at" timestamp without time zone,
    "bottom_ad_one_link" character varying,
    "bottom_ad_one_file_name" character varying,
    "bottom_ad_one_content_type" character varying,
    "bottom_ad_one_file_size" integer,
    "bottom_ad_one_updated_at" timestamp without time zone,
    "bottom_ad_two_link" character varying,
    "bottom_ad_two_file_name" character varying,
    "bottom_ad_two_content_type" character varying,
    "bottom_ad_two_file_size" integer,
    "bottom_ad_two_updated_at" timestamp without time zone,
    "bottom_ad_three_link" character varying,
    "bottom_ad_three_file_name" character varying,
    "bottom_ad_three_content_type" character varying,
    "bottom_ad_three_file_size" integer,
    "bottom_ad_three_updated_at" timestamp without time zone,
    "main_image_file_name" character varying,
    "main_image_content_type" character varying,
    "main_image_file_size" integer,
    "main_image_updated_at" timestamp without time zone
);
    DROP TABLE "public"."blogs";
       public         jyvipeultswzys    false    7            �            1259    8060678    blogs_id_seq    SEQUENCE     p   CREATE SEQUENCE "blogs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."blogs_id_seq";
       public       jyvipeultswzys    false    203    7            d           0    0    blogs_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "blogs_id_seq" OWNED BY "blogs"."id";
            public       jyvipeultswzys    false    202            �            1259    8060716 
   categories    TABLE     �  CREATE TABLE "categories" (
    "id" bigint NOT NULL,
    "name" character varying,
    "category_slug" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "image_file_name" character varying,
    "image_content_type" character varying,
    "image_file_size" integer,
    "image_updated_at" timestamp without time zone
);
 "   DROP TABLE "public"."categories";
       public         jyvipeultswzys    false    7            �            1259    8060714    categories_id_seq    SEQUENCE     u   CREATE SEQUENCE "categories_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE "public"."categories_id_seq";
       public       jyvipeultswzys    false    209    7            e           0    0    categories_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE "categories_id_seq" OWNED BY "categories"."id";
            public       jyvipeultswzys    false    208            �            1259    8060667    ckeditor_assets    TABLE     �  CREATE TABLE "ckeditor_assets" (
    "id" bigint NOT NULL,
    "data_file_name" character varying NOT NULL,
    "data_content_type" character varying,
    "data_file_size" integer,
    "assetable_id" integer,
    "assetable_type" character varying(30),
    "type" character varying(30),
    "width" integer,
    "height" integer,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 '   DROP TABLE "public"."ckeditor_assets";
       public         jyvipeultswzys    false    7            �            1259    8060665    ckeditor_assets_id_seq    SEQUENCE     z   CREATE SEQUENCE "ckeditor_assets_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE "public"."ckeditor_assets_id_seq";
       public       jyvipeultswzys    false    201    7            f           0    0    ckeditor_assets_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE "ckeditor_assets_id_seq" OWNED BY "ckeditor_assets"."id";
            public       jyvipeultswzys    false    200            �            1259    8064550    contacts    TABLE     :  CREATE TABLE "contacts" (
    "id" bigint NOT NULL,
    "name" character varying,
    "email" character varying,
    "phone_number" character varying,
    "message" "text",
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "zip" character varying
);
     DROP TABLE "public"."contacts";
       public         jyvipeultswzys    false    7            �            1259    8064548    contacts_id_seq    SEQUENCE     s   CREATE SEQUENCE "contacts_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE "public"."contacts_id_seq";
       public       jyvipeultswzys    false    7    217            g           0    0    contacts_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE "contacts_id_seq" OWNED BY "contacts"."id";
            public       jyvipeultswzys    false    216            �            1259    8060749    event_images    TABLE     `  CREATE TABLE "event_images" (
    "id" bigint NOT NULL,
    "pic_file_name" character varying,
    "pic_content_type" character varying,
    "pic_file_size" integer,
    "pic_updated_at" timestamp without time zone,
    "position" integer,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 $   DROP TABLE "public"."event_images";
       public         jyvipeultswzys    false    7            �            1259    8060747    event_images_id_seq    SEQUENCE     w   CREATE SEQUENCE "event_images_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE "public"."event_images_id_seq";
       public       jyvipeultswzys    false    7    215            h           0    0    event_images_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE "event_images_id_seq" OWNED BY "event_images"."id";
            public       jyvipeultswzys    false    214            �            1259    8115706    event_indices    TABLE     c  CREATE TABLE "event_indices" (
    "id" bigint NOT NULL,
    "bottom_ad_one_file_name" character varying,
    "bottom_ad_one_content_type" character varying,
    "bottom_ad_one_file_size" integer,
    "bottom_ad_one_updated_at" timestamp without time zone,
    "bottom_ad_two_file_name" character varying,
    "bottom_ad_two_content_type" character varying,
    "bottom_ad_two_file_size" integer,
    "bottom_ad_two_updated_at" timestamp without time zone,
    "bottom_ad_three_file_name" character varying,
    "bottom_ad_three_content_type" character varying,
    "bottom_ad_three_file_size" integer,
    "bottom_ad_three_updated_at" timestamp without time zone,
    "side_ad_one_file_name" character varying,
    "side_ad_one_content_type" character varying,
    "side_ad_one_file_size" integer,
    "side_ad_one_updated_at" timestamp without time zone,
    "side_ad_two_file_name" character varying,
    "side_ad_two_content_type" character varying,
    "side_ad_two_file_size" integer,
    "side_ad_two_updated_at" timestamp without time zone,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "bottom_ad_one_link" character varying,
    "bottom_ad_two_link" character varying,
    "bottom_ad_three_link" character varying,
    "side_ad_one_link" character varying,
    "side_ad_two_link" character varying
);
 %   DROP TABLE "public"."event_indices";
       public         jyvipeultswzys    false    7            �            1259    8115704    event_indices_id_seq    SEQUENCE     x   CREATE SEQUENCE "event_indices_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE "public"."event_indices_id_seq";
       public       jyvipeultswzys    false    222    7            i           0    0    event_indices_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE "event_indices_id_seq" OWNED BY "event_indices"."id";
            public       jyvipeultswzys    false    221            �            1259    8060727    events    TABLE     m  CREATE TABLE "events" (
    "id" bigint NOT NULL,
    "city" character varying,
    "main_image_file_name" character varying,
    "main_image_content_type" character varying,
    "main_image_file_size" integer,
    "main_image_updated_at" timestamp without time zone,
    "event_description" "text",
    "event_date" timestamp without time zone,
    "address" character varying,
    "latitude" double precision,
    "longitude" double precision,
    "agenda_title" character varying,
    "agenda_description" character varying,
    "eventbrite_link" character varying,
    "side_ad_one_image_file_name" character varying,
    "side_ad_one_image_content_type" character varying,
    "side_ad_one_image_file_size" integer,
    "side_ad_one_image_updated_at" timestamp without time zone,
    "side_ad_one_link" character varying,
    "ad_one_image_file_name" character varying,
    "ad_one_image_content_type" character varying,
    "ad_one_image_file_size" integer,
    "ad_one_image_updated_at" timestamp without time zone,
    "ad_one_link" character varying,
    "ad_two_image_file_name" character varying,
    "ad_two_image_content_type" character varying,
    "ad_two_image_file_size" integer,
    "ad_two_image_updated_at" timestamp without time zone,
    "ad_two_link" character varying,
    "ad_three_image_file_name" character varying,
    "ad_three_image_content_type" character varying,
    "ad_three_image_file_size" integer,
    "ad_three_image_updated_at" timestamp without time zone,
    "ad_three_link" character varying,
    "title_for_slug" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "location_description" "text",
    "meta_title" character varying,
    "meta_description" character varying,
    "cover_image_file_name" character varying,
    "cover_image_content_type" character varying,
    "cover_image_file_size" integer,
    "cover_image_updated_at" timestamp without time zone,
    "icon_file_name" character varying,
    "icon_content_type" character varying,
    "icon_file_size" integer,
    "icon_updated_at" timestamp without time zone
);
    DROP TABLE "public"."events";
       public         jyvipeultswzys    false    7            �            1259    8060725    events_id_seq    SEQUENCE     q   CREATE SEQUENCE "events_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE "public"."events_id_seq";
       public       jyvipeultswzys    false    7    211            j           0    0    events_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE "events_id_seq" OWNED BY "events"."id";
            public       jyvipeultswzys    false    210            �            1259    8111252    events_partners    TABLE     e   CREATE TABLE "events_partners" (
    "partner_id" bigint NOT NULL,
    "event_id" bigint NOT NULL
);
 '   DROP TABLE "public"."events_partners";
       public         jyvipeultswzys    false    7            �            1259    8060738    faqs    TABLE     �   CREATE TABLE "faqs" (
    "id" bigint NOT NULL,
    "question" character varying,
    "answer" "text",
    "event_id" integer,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
    DROP TABLE "public"."faqs";
       public         jyvipeultswzys    false    7            �            1259    8060736    faqs_id_seq    SEQUENCE     o   CREATE SEQUENCE "faqs_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE "public"."faqs_id_seq";
       public       jyvipeultswzys    false    213    7            k           0    0    faqs_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE "faqs_id_seq" OWNED BY "faqs"."id";
            public       jyvipeultswzys    false    212            �            1259    8130351    home_page_sliders    TABLE     �  CREATE TABLE "home_page_sliders" (
    "id" bigint NOT NULL,
    "image1_link" character varying,
    "image1_file_name" character varying,
    "image1_content_type" character varying,
    "image1_file_size" integer,
    "image1_updated_at" timestamp without time zone,
    "image2_link" character varying,
    "image2_file_name" character varying,
    "image2_content_type" character varying,
    "image2_file_size" integer,
    "image2_updated_at" timestamp without time zone,
    "image3_link" character varying,
    "image3_file_name" character varying,
    "image3_content_type" character varying,
    "image3_file_size" integer,
    "image3_updated_at" timestamp without time zone,
    "image4_link" character varying,
    "image4_file_name" character varying,
    "image4_content_type" character varying,
    "image4_file_size" integer,
    "image4_updated_at" timestamp without time zone,
    "image5_link" character varying,
    "image5_file_name" character varying,
    "image5_content_type" character varying,
    "image5_file_size" integer,
    "image5_updated_at" timestamp without time zone,
    "image6_link" character varying,
    "image6_file_name" character varying,
    "image6_content_type" character varying,
    "image6_file_size" integer,
    "image6_updated_at" timestamp without time zone,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 )   DROP TABLE "public"."home_page_sliders";
       public         jyvipeultswzys    false    7            �            1259    8130349    home_page_sliders_id_seq    SEQUENCE     |   CREATE SEQUENCE "home_page_sliders_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE "public"."home_page_sliders_id_seq";
       public       jyvipeultswzys    false    7    236            l           0    0    home_page_sliders_id_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE "home_page_sliders_id_seq" OWNED BY "home_page_sliders"."id";
            public       jyvipeultswzys    false    235            �            1259    8117039 
   home_pages    TABLE     �  CREATE TABLE "home_pages" (
    "id" bigint NOT NULL,
    "description" "text",
    "main_image_file_name" character varying,
    "main_image_content_type" character varying,
    "main_image_file_size" integer,
    "main_image_updated_at" timestamp without time zone,
    "bottom_ad_one_file_name" character varying,
    "bottom_ad_one_content_type" character varying,
    "bottom_ad_one_file_size" integer,
    "bottom_ad_one_updated_at" timestamp without time zone,
    "bottom_ad_one_link" character varying,
    "bottom_ad_two_file_name" character varying,
    "bottom_ad_two_content_type" character varying,
    "bottom_ad_two_file_size" integer,
    "bottom_ad_two_updated_at" timestamp without time zone,
    "bottom_ad_two_link" character varying,
    "bottom_ad_three_file_name" character varying,
    "bottom_ad_three_content_type" character varying,
    "bottom_ad_three_file_size" integer,
    "bottom_ad_three_updated_at" timestamp without time zone,
    "bottom_ad_three_link" character varying,
    "meta_title" character varying,
    "meta_description" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 "   DROP TABLE "public"."home_pages";
       public         jyvipeultswzys    false    7            �            1259    8117037    home_pages_id_seq    SEQUENCE     u   CREATE SEQUENCE "home_pages_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE "public"."home_pages_id_seq";
       public       jyvipeultswzys    false    230    7            m           0    0    home_pages_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE "home_pages_id_seq" OWNED BY "home_pages"."id";
            public       jyvipeultswzys    false    229            �            1259    8060692    images    TABLE     �  CREATE TABLE "images" (
    "id" bigint NOT NULL,
    "imageable_type" character varying,
    "imageable_id" bigint,
    "name" character varying,
    "pic_file_name" character varying,
    "pic_content_type" character varying,
    "pic_file_size" integer,
    "pic_updated_at" timestamp without time zone,
    "primary" boolean,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "position" integer
);
    DROP TABLE "public"."images";
       public         jyvipeultswzys    false    7            �            1259    8060690    images_id_seq    SEQUENCE     q   CREATE SEQUENCE "images_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE "public"."images_id_seq";
       public       jyvipeultswzys    false    7    205            n           0    0    images_id_seq    SEQUENCE OWNED BY     7   ALTER SEQUENCE "images_id_seq" OWNED BY "images"."id";
            public       jyvipeultswzys    false    204            �            1259    8116832    partner_with_us    TABLE        CREATE TABLE "partner_with_us" (
    "id" bigint NOT NULL,
    "main_image_file_name" character varying,
    "main_image_content_type" character varying,
    "main_image_file_size" integer,
    "main_image_updated_at" timestamp without time zone,
    "description" "text",
    "testimonial" character varying,
    "testimonial_author" character varying,
    "meta_title" character varying,
    "meta_description" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 '   DROP TABLE "public"."partner_with_us";
       public         jyvipeultswzys    false    7            �            1259    8116830    partner_with_us_id_seq    SEQUENCE     z   CREATE SEQUENCE "partner_with_us_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE "public"."partner_with_us_id_seq";
       public       jyvipeultswzys    false    7    228            o           0    0    partner_with_us_id_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE "partner_with_us_id_seq" OWNED BY "partner_with_us"."id";
            public       jyvipeultswzys    false    227            �            1259    8111243    partners    TABLE     \  CREATE TABLE "partners" (
    "id" bigint NOT NULL,
    "name" character varying,
    "weblink" character varying,
    "description" "text",
    "testimonial" character varying,
    "testimonial_author" character varying,
    "title_for_slug" character varying,
    "main_image_file_name" character varying,
    "main_image_content_type" character varying,
    "main_image_file_size" integer,
    "main_image_updated_at" timestamp without time zone,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "meta_description" character varying
);
     DROP TABLE "public"."partners";
       public         jyvipeultswzys    false    7            �            1259    8111241    partners_id_seq    SEQUENCE     s   CREATE SEQUENCE "partners_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 *   DROP SEQUENCE "public"."partners_id_seq";
       public       jyvipeultswzys    false    7    219            p           0    0    partners_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE "partners_id_seq" OWNED BY "partners"."id";
            public       jyvipeultswzys    false    218            �            1259    8060704    press    TABLE       CREATE TABLE "press" (
    "id" bigint NOT NULL,
    "title" character varying,
    "content" "text",
    "content_index" "text",
    "title_for_slug" character varying,
    "pubdate" timestamp without time zone,
    "meta_description" character varying,
    "meta_keywords" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "side_ad_link" character varying,
    "side_ad_file_name" character varying,
    "side_ad_content_type" character varying,
    "side_ad_file_size" integer,
    "side_ad_updated_at" timestamp without time zone,
    "bottom_ad_one_link" character varying,
    "bottom_ad_one_file_name" character varying,
    "bottom_ad_one_content_type" character varying,
    "bottom_ad_one_file_size" integer,
    "bottom_ad_one_updated_at" timestamp without time zone,
    "bottom_ad_two_link" character varying,
    "bottom_ad_two_file_name" character varying,
    "bottom_ad_two_content_type" character varying,
    "bottom_ad_two_file_size" integer,
    "bottom_ad_two_updated_at" timestamp without time zone,
    "bottom_ad_three_link" character varying,
    "bottom_ad_three_file_name" character varying,
    "bottom_ad_three_content_type" character varying,
    "bottom_ad_three_file_size" integer,
    "bottom_ad_three_updated_at" timestamp without time zone,
    "main_image_file_name" character varying,
    "main_image_content_type" character varying,
    "main_image_file_size" integer,
    "main_image_updated_at" timestamp without time zone
);
    DROP TABLE "public"."press";
       public         jyvipeultswzys    false    7            �            1259    8060702    press_id_seq    SEQUENCE     p   CREATE SEQUENCE "press_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."press_id_seq";
       public       jyvipeultswzys    false    7    207            q           0    0    press_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "press_id_seq" OWNED BY "press"."id";
            public       jyvipeultswzys    false    206            �            1259    8127621    press_indices    TABLE     e  CREATE TABLE "press_indices" (
    "id" bigint NOT NULL,
    "bottom_ad_one_file_name" character varying,
    "bottom_ad_one_content_type" character varying,
    "bottom_ad_one_file_size" integer,
    "bottom_ad_one_updated_at" timestamp without time zone,
    "bottom_ad_two_file_name" character varying,
    "bottom_ad_two_content_type" character varying,
    "bottom_ad_two_file_size" integer,
    "bottom_ad_two_updated_at" timestamp without time zone,
    "bottom_ad_three_file_name" character varying,
    "bottom_ad_three_content_type" character varying,
    "bottom_ad_three_file_size" integer,
    "bottom_ad_three_updated_at" timestamp without time zone,
    "side_ad_file_name" character varying,
    "side_ad_content_type" character varying,
    "side_ad_file_size" integer,
    "side_ad_updated_at" timestamp without time zone,
    "side_ad_link" character varying,
    "bottom_ad_one_link" character varying,
    "bottom_ad_two_link" character varying,
    "bottom_ad_three_link" character varying,
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL
);
 %   DROP TABLE "public"."press_indices";
       public         jyvipeultswzys    false    7            �            1259    8127619    press_indices_id_seq    SEQUENCE     x   CREATE SEQUENCE "press_indices_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE "public"."press_indices_id_seq";
       public       jyvipeultswzys    false    234    7            r           0    0    press_indices_id_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE "press_indices_id_seq" OWNED BY "press_indices"."id";
            public       jyvipeultswzys    false    233            �            1259    5942570    schema_migrations    TABLE     O   CREATE TABLE "schema_migrations" (
    "version" character varying NOT NULL
);
 )   DROP TABLE "public"."schema_migrations";
       public         jyvipeultswzys    false    7            �            1259    8060651    users    TABLE     �  CREATE TABLE "users" (
    "id" bigint NOT NULL,
    "email" character varying DEFAULT ''::character varying NOT NULL,
    "encrypted_password" character varying DEFAULT ''::character varying NOT NULL,
    "reset_password_token" character varying,
    "reset_password_sent_at" timestamp without time zone,
    "remember_created_at" timestamp without time zone,
    "sign_in_count" integer DEFAULT 0 NOT NULL,
    "current_sign_in_at" timestamp without time zone,
    "last_sign_in_at" timestamp without time zone,
    "current_sign_in_ip" "inet",
    "last_sign_in_ip" "inet",
    "created_at" timestamp without time zone NOT NULL,
    "updated_at" timestamp without time zone NOT NULL,
    "admin" boolean
);
    DROP TABLE "public"."users";
       public         jyvipeultswzys    false    7            �            1259    8060649    users_id_seq    SEQUENCE     p   CREATE SEQUENCE "users_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE "public"."users_id_seq";
       public       jyvipeultswzys    false    7    199            s           0    0    users_id_seq    SEQUENCE OWNED BY     5   ALTER SEQUENCE "users_id_seq" OWNED BY "users"."id";
            public       jyvipeultswzys    false    198            ~           2604    8116279    about_amies id    DEFAULT     l   ALTER TABLE ONLY "about_amies" ALTER COLUMN "id" SET DEFAULT "nextval"('"about_amies_id_seq"'::"regclass");
 C   ALTER TABLE "public"."about_amies" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    224    223    224                       2604    8116481    about_moabs id    DEFAULT     l   ALTER TABLE ONLY "about_moabs" ALTER COLUMN "id" SET DEFAULT "nextval"('"about_moabs_id_seq"'::"regclass");
 C   ALTER TABLE "public"."about_moabs" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    225    226    226            �           2604    8127613    blog_indices id    DEFAULT     n   ALTER TABLE ONLY "blog_indices" ALTER COLUMN "id" SET DEFAULT "nextval"('"blog_indices_id_seq"'::"regclass");
 D   ALTER TABLE "public"."blog_indices" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    231    232    232            t           2604    8060683    blogs id    DEFAULT     `   ALTER TABLE ONLY "blogs" ALTER COLUMN "id" SET DEFAULT "nextval"('"blogs_id_seq"'::"regclass");
 =   ALTER TABLE "public"."blogs" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    202    203    203            w           2604    8060719    categories id    DEFAULT     j   ALTER TABLE ONLY "categories" ALTER COLUMN "id" SET DEFAULT "nextval"('"categories_id_seq"'::"regclass");
 B   ALTER TABLE "public"."categories" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    209    208    209            s           2604    8060670    ckeditor_assets id    DEFAULT     t   ALTER TABLE ONLY "ckeditor_assets" ALTER COLUMN "id" SET DEFAULT "nextval"('"ckeditor_assets_id_seq"'::"regclass");
 G   ALTER TABLE "public"."ckeditor_assets" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    200    201    201            {           2604    8064553    contacts id    DEFAULT     f   ALTER TABLE ONLY "contacts" ALTER COLUMN "id" SET DEFAULT "nextval"('"contacts_id_seq"'::"regclass");
 @   ALTER TABLE "public"."contacts" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    217    216    217            z           2604    8060752    event_images id    DEFAULT     n   ALTER TABLE ONLY "event_images" ALTER COLUMN "id" SET DEFAULT "nextval"('"event_images_id_seq"'::"regclass");
 D   ALTER TABLE "public"."event_images" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    215    214    215            }           2604    8115709    event_indices id    DEFAULT     p   ALTER TABLE ONLY "event_indices" ALTER COLUMN "id" SET DEFAULT "nextval"('"event_indices_id_seq"'::"regclass");
 E   ALTER TABLE "public"."event_indices" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    221    222    222            x           2604    8060730 	   events id    DEFAULT     b   ALTER TABLE ONLY "events" ALTER COLUMN "id" SET DEFAULT "nextval"('"events_id_seq"'::"regclass");
 >   ALTER TABLE "public"."events" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    211    210    211            y           2604    8060741    faqs id    DEFAULT     ^   ALTER TABLE ONLY "faqs" ALTER COLUMN "id" SET DEFAULT "nextval"('"faqs_id_seq"'::"regclass");
 <   ALTER TABLE "public"."faqs" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    212    213    213            �           2604    8130354    home_page_sliders id    DEFAULT     x   ALTER TABLE ONLY "home_page_sliders" ALTER COLUMN "id" SET DEFAULT "nextval"('"home_page_sliders_id_seq"'::"regclass");
 I   ALTER TABLE "public"."home_page_sliders" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    235    236    236            �           2604    8117042    home_pages id    DEFAULT     j   ALTER TABLE ONLY "home_pages" ALTER COLUMN "id" SET DEFAULT "nextval"('"home_pages_id_seq"'::"regclass");
 B   ALTER TABLE "public"."home_pages" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    229    230    230            u           2604    8060695 	   images id    DEFAULT     b   ALTER TABLE ONLY "images" ALTER COLUMN "id" SET DEFAULT "nextval"('"images_id_seq"'::"regclass");
 >   ALTER TABLE "public"."images" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    205    204    205            �           2604    8116835    partner_with_us id    DEFAULT     t   ALTER TABLE ONLY "partner_with_us" ALTER COLUMN "id" SET DEFAULT "nextval"('"partner_with_us_id_seq"'::"regclass");
 G   ALTER TABLE "public"."partner_with_us" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    227    228    228            |           2604    8111246    partners id    DEFAULT     f   ALTER TABLE ONLY "partners" ALTER COLUMN "id" SET DEFAULT "nextval"('"partners_id_seq"'::"regclass");
 @   ALTER TABLE "public"."partners" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    219    218    219            v           2604    8060707    press id    DEFAULT     `   ALTER TABLE ONLY "press" ALTER COLUMN "id" SET DEFAULT "nextval"('"press_id_seq"'::"regclass");
 =   ALTER TABLE "public"."press" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    206    207    207            �           2604    8127624    press_indices id    DEFAULT     p   ALTER TABLE ONLY "press_indices" ALTER COLUMN "id" SET DEFAULT "nextval"('"press_indices_id_seq"'::"regclass");
 E   ALTER TABLE "public"."press_indices" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    233    234    234            o           2604    8060654    users id    DEFAULT     `   ALTER TABLE ONLY "users" ALTER COLUMN "id" SET DEFAULT "nextval"('"users_id_seq"'::"regclass");
 =   ALTER TABLE "public"."users" ALTER COLUMN "id" DROP DEFAULT;
       public       jyvipeultswzys    false    198    199    199            M          0    8116276    about_amies 
   TABLE DATA               �  COPY "about_amies" ("id", "meet_amy", "background_img_file_name", "background_img_content_type", "background_img_file_size", "background_img_updated_at", "moabs_experience", "bottom_ad_one_file_name", "bottom_ad_one_content_type", "bottom_ad_one_file_size", "bottom_ad_one_updated_at", "bottom_ad_two_file_name", "bottom_ad_two_content_type", "bottom_ad_two_file_size", "bottom_ad_two_updated_at", "bottom_ad_three_file_name", "bottom_ad_three_content_type", "bottom_ad_three_file_size", "bottom_ad_three_updated_at", "meta_title", "meta_description", "created_at", "updated_at", "bottom_ad_one_link", "bottom_ad_two_link", "bottom_ad_three_link") FROM stdin;
    public       jyvipeultswzys    false    224            O          0    8116478    about_moabs 
   TABLE DATA               �   COPY "about_moabs" ("id", "main_image_file_name", "main_image_content_type", "main_image_file_size", "main_image_updated_at", "description", "meta_title", "meta_description", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    226            2          0    5942578    ar_internal_metadata 
   TABLE DATA               U   COPY "ar_internal_metadata" ("key", "value", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    197            U          0    8127610    blog_indices 
   TABLE DATA               G  COPY "blog_indices" ("id", "bottom_ad_one_file_name", "bottom_ad_one_content_type", "bottom_ad_one_file_size", "bottom_ad_one_updated_at", "bottom_ad_two_file_name", "bottom_ad_two_content_type", "bottom_ad_two_file_size", "bottom_ad_two_updated_at", "bottom_ad_three_file_name", "bottom_ad_three_content_type", "bottom_ad_three_file_size", "bottom_ad_three_updated_at", "side_ad_file_name", "side_ad_content_type", "side_ad_file_size", "side_ad_updated_at", "side_ad_link", "bottom_ad_one_link", "bottom_ad_two_link", "bottom_ad_three_link", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    232            8          0    8060680    blogs 
   TABLE DATA                 COPY "blogs" ("id", "title", "content", "content_index", "title_for_slug", "pubdate", "meta_description", "meta_keywords", "created_at", "updated_at", "category_id", "side_ad_link", "side_ad_file_name", "side_ad_content_type", "side_ad_file_size", "side_ad_updated_at", "bottom_ad_one_link", "bottom_ad_one_file_name", "bottom_ad_one_content_type", "bottom_ad_one_file_size", "bottom_ad_one_updated_at", "bottom_ad_two_link", "bottom_ad_two_file_name", "bottom_ad_two_content_type", "bottom_ad_two_file_size", "bottom_ad_two_updated_at", "bottom_ad_three_link", "bottom_ad_three_file_name", "bottom_ad_three_content_type", "bottom_ad_three_file_size", "bottom_ad_three_updated_at", "main_image_file_name", "main_image_content_type", "main_image_file_size", "main_image_updated_at") FROM stdin;
    public       jyvipeultswzys    false    203            >          0    8060716 
   categories 
   TABLE DATA               �   COPY "categories" ("id", "name", "category_slug", "created_at", "updated_at", "image_file_name", "image_content_type", "image_file_size", "image_updated_at") FROM stdin;
    public       jyvipeultswzys    false    209            6          0    8060667    ckeditor_assets 
   TABLE DATA               �   COPY "ckeditor_assets" ("id", "data_file_name", "data_content_type", "data_file_size", "assetable_id", "assetable_type", "type", "width", "height", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    201            F          0    8064550    contacts 
   TABLE DATA               r   COPY "contacts" ("id", "name", "email", "phone_number", "message", "created_at", "updated_at", "zip") FROM stdin;
    public       jyvipeultswzys    false    217            D          0    8060749    event_images 
   TABLE DATA               �   COPY "event_images" ("id", "pic_file_name", "pic_content_type", "pic_file_size", "pic_updated_at", "position", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    215            K          0    8115706    event_indices 
   TABLE DATA               �  COPY "event_indices" ("id", "bottom_ad_one_file_name", "bottom_ad_one_content_type", "bottom_ad_one_file_size", "bottom_ad_one_updated_at", "bottom_ad_two_file_name", "bottom_ad_two_content_type", "bottom_ad_two_file_size", "bottom_ad_two_updated_at", "bottom_ad_three_file_name", "bottom_ad_three_content_type", "bottom_ad_three_file_size", "bottom_ad_three_updated_at", "side_ad_one_file_name", "side_ad_one_content_type", "side_ad_one_file_size", "side_ad_one_updated_at", "side_ad_two_file_name", "side_ad_two_content_type", "side_ad_two_file_size", "side_ad_two_updated_at", "created_at", "updated_at", "bottom_ad_one_link", "bottom_ad_two_link", "bottom_ad_three_link", "side_ad_one_link", "side_ad_two_link") FROM stdin;
    public       jyvipeultswzys    false    222            @          0    8060727    events 
   TABLE DATA               8  COPY "events" ("id", "city", "main_image_file_name", "main_image_content_type", "main_image_file_size", "main_image_updated_at", "event_description", "event_date", "address", "latitude", "longitude", "agenda_title", "agenda_description", "eventbrite_link", "side_ad_one_image_file_name", "side_ad_one_image_content_type", "side_ad_one_image_file_size", "side_ad_one_image_updated_at", "side_ad_one_link", "ad_one_image_file_name", "ad_one_image_content_type", "ad_one_image_file_size", "ad_one_image_updated_at", "ad_one_link", "ad_two_image_file_name", "ad_two_image_content_type", "ad_two_image_file_size", "ad_two_image_updated_at", "ad_two_link", "ad_three_image_file_name", "ad_three_image_content_type", "ad_three_image_file_size", "ad_three_image_updated_at", "ad_three_link", "title_for_slug", "created_at", "updated_at", "location_description", "meta_title", "meta_description", "cover_image_file_name", "cover_image_content_type", "cover_image_file_size", "cover_image_updated_at", "icon_file_name", "icon_content_type", "icon_file_size", "icon_updated_at") FROM stdin;
    public       jyvipeultswzys    false    211            I          0    8111252    events_partners 
   TABLE DATA               >   COPY "events_partners" ("partner_id", "event_id") FROM stdin;
    public       jyvipeultswzys    false    220            B          0    8060738    faqs 
   TABLE DATA               ]   COPY "faqs" ("id", "question", "answer", "event_id", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    213            Y          0    8130351    home_page_sliders 
   TABLE DATA               �  COPY "home_page_sliders" ("id", "image1_link", "image1_file_name", "image1_content_type", "image1_file_size", "image1_updated_at", "image2_link", "image2_file_name", "image2_content_type", "image2_file_size", "image2_updated_at", "image3_link", "image3_file_name", "image3_content_type", "image3_file_size", "image3_updated_at", "image4_link", "image4_file_name", "image4_content_type", "image4_file_size", "image4_updated_at", "image5_link", "image5_file_name", "image5_content_type", "image5_file_size", "image5_updated_at", "image6_link", "image6_file_name", "image6_content_type", "image6_file_size", "image6_updated_at", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    236            S          0    8117039 
   home_pages 
   TABLE DATA               r  COPY "home_pages" ("id", "description", "main_image_file_name", "main_image_content_type", "main_image_file_size", "main_image_updated_at", "bottom_ad_one_file_name", "bottom_ad_one_content_type", "bottom_ad_one_file_size", "bottom_ad_one_updated_at", "bottom_ad_one_link", "bottom_ad_two_file_name", "bottom_ad_two_content_type", "bottom_ad_two_file_size", "bottom_ad_two_updated_at", "bottom_ad_two_link", "bottom_ad_three_file_name", "bottom_ad_three_content_type", "bottom_ad_three_file_size", "bottom_ad_three_updated_at", "bottom_ad_three_link", "meta_title", "meta_description", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    230            :          0    8060692    images 
   TABLE DATA               �   COPY "images" ("id", "imageable_type", "imageable_id", "name", "pic_file_name", "pic_content_type", "pic_file_size", "pic_updated_at", "primary", "created_at", "updated_at", "position") FROM stdin;
    public       jyvipeultswzys    false    205            Q          0    8116832    partner_with_us 
   TABLE DATA                  COPY "partner_with_us" ("id", "main_image_file_name", "main_image_content_type", "main_image_file_size", "main_image_updated_at", "description", "testimonial", "testimonial_author", "meta_title", "meta_description", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    228            H          0    8111243    partners 
   TABLE DATA                 COPY "partners" ("id", "name", "weblink", "description", "testimonial", "testimonial_author", "title_for_slug", "main_image_file_name", "main_image_content_type", "main_image_file_size", "main_image_updated_at", "created_at", "updated_at", "meta_description") FROM stdin;
    public       jyvipeultswzys    false    219            <          0    8060704    press 
   TABLE DATA                 COPY "press" ("id", "title", "content", "content_index", "title_for_slug", "pubdate", "meta_description", "meta_keywords", "created_at", "updated_at", "side_ad_link", "side_ad_file_name", "side_ad_content_type", "side_ad_file_size", "side_ad_updated_at", "bottom_ad_one_link", "bottom_ad_one_file_name", "bottom_ad_one_content_type", "bottom_ad_one_file_size", "bottom_ad_one_updated_at", "bottom_ad_two_link", "bottom_ad_two_file_name", "bottom_ad_two_content_type", "bottom_ad_two_file_size", "bottom_ad_two_updated_at", "bottom_ad_three_link", "bottom_ad_three_file_name", "bottom_ad_three_content_type", "bottom_ad_three_file_size", "bottom_ad_three_updated_at", "main_image_file_name", "main_image_content_type", "main_image_file_size", "main_image_updated_at") FROM stdin;
    public       jyvipeultswzys    false    207            W          0    8127621    press_indices 
   TABLE DATA               H  COPY "press_indices" ("id", "bottom_ad_one_file_name", "bottom_ad_one_content_type", "bottom_ad_one_file_size", "bottom_ad_one_updated_at", "bottom_ad_two_file_name", "bottom_ad_two_content_type", "bottom_ad_two_file_size", "bottom_ad_two_updated_at", "bottom_ad_three_file_name", "bottom_ad_three_content_type", "bottom_ad_three_file_size", "bottom_ad_three_updated_at", "side_ad_file_name", "side_ad_content_type", "side_ad_file_size", "side_ad_updated_at", "side_ad_link", "bottom_ad_one_link", "bottom_ad_two_link", "bottom_ad_three_link", "created_at", "updated_at") FROM stdin;
    public       jyvipeultswzys    false    234            1          0    5942570    schema_migrations 
   TABLE DATA               1   COPY "schema_migrations" ("version") FROM stdin;
    public       jyvipeultswzys    false    196            4          0    8060651    users 
   TABLE DATA                 COPY "users" ("id", "email", "encrypted_password", "reset_password_token", "reset_password_sent_at", "remember_created_at", "sign_in_count", "current_sign_in_at", "last_sign_in_at", "current_sign_in_ip", "last_sign_in_ip", "created_at", "updated_at", "admin") FROM stdin;
    public       jyvipeultswzys    false    199            t           0    0    about_amies_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('"about_amies_id_seq"', 2, true);
            public       jyvipeultswzys    false    223            u           0    0    about_moabs_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('"about_moabs_id_seq"', 1, true);
            public       jyvipeultswzys    false    225            v           0    0    blog_indices_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('"blog_indices_id_seq"', 1, true);
            public       jyvipeultswzys    false    231            w           0    0    blogs_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('"blogs_id_seq"', 13, true);
            public       jyvipeultswzys    false    202            x           0    0    categories_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"categories_id_seq"', 4, true);
            public       jyvipeultswzys    false    208            y           0    0    ckeditor_assets_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('"ckeditor_assets_id_seq"', 4, true);
            public       jyvipeultswzys    false    200            z           0    0    contacts_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('"contacts_id_seq"', 39, true);
            public       jyvipeultswzys    false    216            {           0    0    event_images_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('"event_images_id_seq"', 1, false);
            public       jyvipeultswzys    false    214            |           0    0    event_indices_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('"event_indices_id_seq"', 1, true);
            public       jyvipeultswzys    false    221            }           0    0    events_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('"events_id_seq"', 11, true);
            public       jyvipeultswzys    false    210            ~           0    0    faqs_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"faqs_id_seq"', 73, true);
            public       jyvipeultswzys    false    212                       0    0    home_page_sliders_id_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('"home_page_sliders_id_seq"', 1, true);
            public       jyvipeultswzys    false    235            �           0    0    home_pages_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('"home_pages_id_seq"', 1, true);
            public       jyvipeultswzys    false    229            �           0    0    images_id_seq    SEQUENCE SET     7   SELECT pg_catalog.setval('"images_id_seq"', 17, true);
            public       jyvipeultswzys    false    204            �           0    0    partner_with_us_id_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('"partner_with_us_id_seq"', 1, true);
            public       jyvipeultswzys    false    227            �           0    0    partners_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('"partners_id_seq"', 26, true);
            public       jyvipeultswzys    false    218            �           0    0    press_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"press_id_seq"', 3, true);
            public       jyvipeultswzys    false    206            �           0    0    press_indices_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('"press_indices_id_seq"', 1, true);
            public       jyvipeultswzys    false    233            �           0    0    users_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('"users_id_seq"', 1, true);
            public       jyvipeultswzys    false    198            �           2606    8116284    about_amies about_amies_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY "about_amies"
    ADD CONSTRAINT "about_amies_pkey" PRIMARY KEY ("id");
 L   ALTER TABLE ONLY "public"."about_amies" DROP CONSTRAINT "about_amies_pkey";
       public         jyvipeultswzys    false    224            �           2606    8116486    about_moabs about_moabs_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY "about_moabs"
    ADD CONSTRAINT "about_moabs_pkey" PRIMARY KEY ("id");
 L   ALTER TABLE ONLY "public"."about_moabs" DROP CONSTRAINT "about_moabs_pkey";
       public         jyvipeultswzys    false    226            �           2606    5942585 .   ar_internal_metadata ar_internal_metadata_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY "ar_internal_metadata"
    ADD CONSTRAINT "ar_internal_metadata_pkey" PRIMARY KEY ("key");
 ^   ALTER TABLE ONLY "public"."ar_internal_metadata" DROP CONSTRAINT "ar_internal_metadata_pkey";
       public         jyvipeultswzys    false    197            �           2606    8127618    blog_indices blog_indices_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY "blog_indices"
    ADD CONSTRAINT "blog_indices_pkey" PRIMARY KEY ("id");
 N   ALTER TABLE ONLY "public"."blog_indices" DROP CONSTRAINT "blog_indices_pkey";
       public         jyvipeultswzys    false    232            �           2606    8060688    blogs blogs_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "blogs"
    ADD CONSTRAINT "blogs_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."blogs" DROP CONSTRAINT "blogs_pkey";
       public         jyvipeultswzys    false    203            �           2606    8060724    categories categories_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY "categories"
    ADD CONSTRAINT "categories_pkey" PRIMARY KEY ("id");
 J   ALTER TABLE ONLY "public"."categories" DROP CONSTRAINT "categories_pkey";
       public         jyvipeultswzys    false    209            �           2606    8060675 $   ckeditor_assets ckeditor_assets_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "ckeditor_assets"
    ADD CONSTRAINT "ckeditor_assets_pkey" PRIMARY KEY ("id");
 T   ALTER TABLE ONLY "public"."ckeditor_assets" DROP CONSTRAINT "ckeditor_assets_pkey";
       public         jyvipeultswzys    false    201            �           2606    8064558    contacts contacts_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "contacts"
    ADD CONSTRAINT "contacts_pkey" PRIMARY KEY ("id");
 F   ALTER TABLE ONLY "public"."contacts" DROP CONSTRAINT "contacts_pkey";
       public         jyvipeultswzys    false    217            �           2606    8060757    event_images event_images_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY "event_images"
    ADD CONSTRAINT "event_images_pkey" PRIMARY KEY ("id");
 N   ALTER TABLE ONLY "public"."event_images" DROP CONSTRAINT "event_images_pkey";
       public         jyvipeultswzys    false    215            �           2606    8115714     event_indices event_indices_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY "event_indices"
    ADD CONSTRAINT "event_indices_pkey" PRIMARY KEY ("id");
 P   ALTER TABLE ONLY "public"."event_indices" DROP CONSTRAINT "event_indices_pkey";
       public         jyvipeultswzys    false    222            �           2606    8060735    events events_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY "events"
    ADD CONSTRAINT "events_pkey" PRIMARY KEY ("id");
 B   ALTER TABLE ONLY "public"."events" DROP CONSTRAINT "events_pkey";
       public         jyvipeultswzys    false    211            �           2606    8060746    faqs faqs_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY "faqs"
    ADD CONSTRAINT "faqs_pkey" PRIMARY KEY ("id");
 >   ALTER TABLE ONLY "public"."faqs" DROP CONSTRAINT "faqs_pkey";
       public         jyvipeultswzys    false    213            �           2606    8130359 (   home_page_sliders home_page_sliders_pkey 
   CONSTRAINT     e   ALTER TABLE ONLY "home_page_sliders"
    ADD CONSTRAINT "home_page_sliders_pkey" PRIMARY KEY ("id");
 X   ALTER TABLE ONLY "public"."home_page_sliders" DROP CONSTRAINT "home_page_sliders_pkey";
       public         jyvipeultswzys    false    236            �           2606    8117047    home_pages home_pages_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY "home_pages"
    ADD CONSTRAINT "home_pages_pkey" PRIMARY KEY ("id");
 J   ALTER TABLE ONLY "public"."home_pages" DROP CONSTRAINT "home_pages_pkey";
       public         jyvipeultswzys    false    230            �           2606    8060700    images images_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY "images"
    ADD CONSTRAINT "images_pkey" PRIMARY KEY ("id");
 B   ALTER TABLE ONLY "public"."images" DROP CONSTRAINT "images_pkey";
       public         jyvipeultswzys    false    205            �           2606    8116840 $   partner_with_us partner_with_us_pkey 
   CONSTRAINT     a   ALTER TABLE ONLY "partner_with_us"
    ADD CONSTRAINT "partner_with_us_pkey" PRIMARY KEY ("id");
 T   ALTER TABLE ONLY "public"."partner_with_us" DROP CONSTRAINT "partner_with_us_pkey";
       public         jyvipeultswzys    false    228            �           2606    8111251    partners partners_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY "partners"
    ADD CONSTRAINT "partners_pkey" PRIMARY KEY ("id");
 F   ALTER TABLE ONLY "public"."partners" DROP CONSTRAINT "partners_pkey";
       public         jyvipeultswzys    false    219            �           2606    8127629     press_indices press_indices_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY "press_indices"
    ADD CONSTRAINT "press_indices_pkey" PRIMARY KEY ("id");
 P   ALTER TABLE ONLY "public"."press_indices" DROP CONSTRAINT "press_indices_pkey";
       public         jyvipeultswzys    false    234            �           2606    8060712    press press_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "press"
    ADD CONSTRAINT "press_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."press" DROP CONSTRAINT "press_pkey";
       public         jyvipeultswzys    false    207            �           2606    5942577 (   schema_migrations schema_migrations_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY "schema_migrations"
    ADD CONSTRAINT "schema_migrations_pkey" PRIMARY KEY ("version");
 X   ALTER TABLE ONLY "public"."schema_migrations" DROP CONSTRAINT "schema_migrations_pkey";
       public         jyvipeultswzys    false    196            �           2606    8060662    users users_pkey 
   CONSTRAINT     M   ALTER TABLE ONLY "users"
    ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
 @   ALTER TABLE ONLY "public"."users" DROP CONSTRAINT "users_pkey";
       public         jyvipeultswzys    false    199            �           1259    8060677    idx_ckeditor_assetable    INDEX     m   CREATE INDEX "idx_ckeditor_assetable" ON "ckeditor_assets" USING "btree" ("assetable_type", "assetable_id");
 .   DROP INDEX "public"."idx_ckeditor_assetable";
       public         jyvipeultswzys    false    201    201            �           1259    8060676    idx_ckeditor_assetable_type    INDEX     z   CREATE INDEX "idx_ckeditor_assetable_type" ON "ckeditor_assets" USING "btree" ("assetable_type", "type", "assetable_id");
 3   DROP INDEX "public"."idx_ckeditor_assetable_type";
       public         jyvipeultswzys    false    201    201    201            �           1259    8060689    index_blogs_on_title_for_slug    INDEX     Z   CREATE INDEX "index_blogs_on_title_for_slug" ON "blogs" USING "btree" ("title_for_slug");
 5   DROP INDEX "public"."index_blogs_on_title_for_slug";
       public         jyvipeultswzys    false    203            �           1259    8111256 0   index_events_partners_on_event_id_and_partner_id    INDEX        CREATE INDEX "index_events_partners_on_event_id_and_partner_id" ON "events_partners" USING "btree" ("event_id", "partner_id");
 H   DROP INDEX "public"."index_events_partners_on_event_id_and_partner_id";
       public         jyvipeultswzys    false    220    220            �           1259    8111255 0   index_events_partners_on_partner_id_and_event_id    INDEX        CREATE INDEX "index_events_partners_on_partner_id_and_event_id" ON "events_partners" USING "btree" ("partner_id", "event_id");
 H   DROP INDEX "public"."index_events_partners_on_partner_id_and_event_id";
       public         jyvipeultswzys    false    220    220            �           1259    8060701 /   index_images_on_imageable_type_and_imageable_id    INDEX     }   CREATE INDEX "index_images_on_imageable_type_and_imageable_id" ON "images" USING "btree" ("imageable_type", "imageable_id");
 G   DROP INDEX "public"."index_images_on_imageable_type_and_imageable_id";
       public         jyvipeultswzys    false    205    205            �           1259    8060713    index_press_on_title_for_slug    INDEX     Z   CREATE INDEX "index_press_on_title_for_slug" ON "press" USING "btree" ("title_for_slug");
 5   DROP INDEX "public"."index_press_on_title_for_slug";
       public         jyvipeultswzys    false    207            �           1259    8060663    index_users_on_email    INDEX     O   CREATE UNIQUE INDEX "index_users_on_email" ON "users" USING "btree" ("email");
 ,   DROP INDEX "public"."index_users_on_email";
       public         jyvipeultswzys    false    199            �           1259    8060664 #   index_users_on_reset_password_token    INDEX     m   CREATE UNIQUE INDEX "index_users_on_reset_password_token" ON "users" USING "btree" ("reset_password_token");
 ;   DROP INDEX "public"."index_users_on_reset_password_token";
       public         jyvipeultswzys    false    199            M   �   x���;�0�z}
.`gwb�8�r�T�nX+"A,>�G|�T���y�tȏ�ؔ���V���vvKQ�\����d%�@`�-�;��X\% |ojO��U��o�h�������� ��m��Ϧ�$g�y�@Ro      O   n   x�m���0 г3EH�M�BL�m#���� �OA>�e���k�&��f)�ڊ���bԖ4���i�� {c���K��kS�r�ޏ�q
�����'�-�l�R�!"E      2   ?   x�K�+�,���M�+�,(�O)M.����420��50"##+Ss+S=s33K<R\1z\\\ �^*      U   �   x���;�0��:Y���Bk�a0F4�2�}kg�=�w~2u����c{��Ҟ�e�y�f��t�Z�Q��G�H�!;�c��{�G���s�`�B舡 ѣs۶5wݾ�P[�K�����~_j�!��5H��3Xk?Uiu�      8      x��}�r�F��o�)`͎Z:���ͫt̠l���=
GL���Q�@�(���/=��؉81��A�Q�$�_f R�L�$E�9;�j�RU��뗙U���W3̵*lP�t����A��*������3;��.��0����Xe�$΢��S�&ȫ20���<�[.�索��Y����rw-��J�'|���-�域�����7����uw��8Y����2�C����y�¤*��]��x�w�ц�N=16�)v�������~�yJ
�P�����f!s���|e�W0S��l����M�yg/x5Se��3�e��H-豃��_�w�`Re٢�>��@+�R�2��$��4�c]����f���VI���3n/�T��:9FG���(�����,�j<�,���2�">v��Q�몼_X�U�=���?>;�ஃ�LeS��K5���KM~L���lʿ�L'%z<�j^��8��-�)H�M�@eG�3g�&��`T����v�ݜ����z���{�����=��_���{��]�X�g#�?���o*���2�D)G2�A�)*4�I<�!]T�5A��4��7x�G�/�
�4�!��	�-�T�I����(�ry5��܈��v<3&	LAͧ��9'���[�>�tݘ#d�V�z�Ԕsd2f��&N~2n�D�Ab���g��,"����h�S�0��bA\��:.��׵Ŭ�N�k�_F9)Tv���픦ne��<gf�����T�w3३1�)��:5�dnO�pHZ5�	�*���4;�D��^p�un}o��Ʈ!�h�1�Z�0j�UMea���/tV���h��c�̕���5�����l=w���3�	I�#�U$����ϊ��r���ygAM�K�PV�Ԁ�e �r��;�i~z&5E*��S]
�9�
m-8=��G���)�lX[�R��Lג�E��/�p��G�N0�2Ȱ��I�JE���^�i���e)�̔��e���i����H���ꊨt.��J4K���h��c[�_�3�;�W�g<��3ݎ}/K)7�δ1�Im%q��e��)�����`�Fz:���+�Ah��*���)�n�U�"m���fI�˸�h��4]�_$�T)��������22�@I���@�(/��1�N4�R$������`�5�Y3D%bR��^�w��Z�M�HC�9ԙ��ˇ��Z������T+�*V9&�k�'�D�9Yf��@x����!���rf�����81�F}�cD���{�ƊI'OTfۼN6/�������V�sO�4���V�J�����ݑ�P��ê��	���X@���PVEf�ӛ,(J�ǚ����&�D�8��r��S���g˔u�Y��62���ņHlgs�N��0����)1�le��#�8d���5�,w��.�� T����X�㑋��b��ڐķ��"tc"C�S^���sX,D�%�@k
�͍+D���Z,H쳵�ҵ8��~V����[VZI�n��;&Dh쓉�}�����)O��Y��
�5�x���Vg�6�I�FwTr*�5'fmZEZ.��$�@�g��%�KRjTM�zh-h��Z�Ll���U"��Fт�ט��]V�<~�I�ƹ)�ȧ��	��%�nL:�}��k����"߁3B�9;�E<��V<z�$!2�u���d$P32��7�mP��|*z��(��_�%kv	�\�!��Q��eHJ�W ���k�س�H�)D�%|n2J&U"V>dZ��|���عcU�YG�c�����/OY_��4|$'mY�B�� �3 2���Q��\�W��e��0al�`�ڔ�W٦��Ћ�:�>K4� i�S�����Z5�a<!��DL	��1�~�zf�ɦ�Qei�&��7&l�sW E�K>IH�l["�\�� &8W�h�cq3
� x�b�g�od�!�n���5g��tX�Ǳ�YS�_���!�񈽣��������J���+���2�iD��>1Y؅5�ݜ_`�_�%2�gвr��`mm�t��@#��[?%�Φ=*����<��cS>�nC�|�BO蹙���,���T����"�ǟ����?��4��i����_�z���ֿ\]������σa��#g]�bu��w��{���wWW��y8_i���/V_<���ֆk���V�Q���f�x<2��+��N{��{2˝[���x��7���X�x��@���5H���%��t�4��yħs�h���Gz���[�YF��q����y��.%����'���ͿZa��K�P����o~�firك5DMw����,Gܺ��a!�O�YzpAVK��2�p�E����]��O˃��s�b�%��?�7��]�ް���ة@
9Ig�PD"^O�di&�*"&�k��,�2\4>�x���W9t��l�$b��dz$fz9�}/��۞�2�U2=<>VY�V�@�4��*{&��Wk���ڠ7X��v��+w���V[��H/0����wv�wv׶��ak{�}k��������ps��2\i��H!M��������?MG*̳�J������pcg��N�6�k��nsksgp��vv�����Zc���$1ӟs��?fY���~�5��1Xl���f��6�>믭�xD�-�mfH���Ή�p\,��퀙��i$Q�rN@�H]Lt�w��
��|� /?�~x�c�;���`��b����G�`<��%e��g�e��.K2�+T�C���TBZ�-�=��8���!��K2"��Ո�69�0�G�a��\�'���d��}laT������X3d�"�H�	}��)�`K$�MLv��⢞�3I:��b��
rĐ�Y/?�I�F�̵8��ү���\hz�����C��׵��G3ƀ�2�A�E��U�*�"���>�ɺؖj���#+P���ڗ�9-�e�"�x�thAh
H�����u����5��4�1�	��P��n4�	Py�`N�����)���sϹ�Bm}��:G�N(9�d����a��1`����@��B�<`#���v���_�DDa	pmޥ�]`Q�a�"I��$U~�dvC�iD����pd�L0Md��y����4��q��n!����o*�c�R����}s�t/���a�{qF���)�~���9�[����q��a_��v�[��rq}�� ���<!���(ض*��y��"h�,/�H�2�G�6 �2��,���.���
|{E�S]LI.��9"ɦ�N�~<*�U�t�K&`��^+9u�\k�os���q������'�0�������	�s��3W��q�3Se��=�38�܀�\��V���u�h��8�@&[uk�iN��Pɳ���� @xa�\B��h����HM
��8G�LUhg���3b;�Į���K���<�>����B� <E�!�D%�F��o�dަ�9U�*�I��|��� ���\�\.[o��b�c�RK���:ג眔'������ͷ������K�����M$e�Y㯱s2��g�5��P?���5���Z�Ǯ�*JuSg�	�Z@���r�����&3�:v�a=������5&9!�[bF��*����B~Y0���9��Y��6���r,}�~��.���&���t5r���<˼��
Ҡ��('*N��$��F�g�hd�T�?��Nj��� p�~�mH�$a�� ��,iA�R^�<^���h�5�x���V��<��1���	�ua������;f>{��_��V9b��38$�G2���ՙ=���vQ>r|�iO�1\�+ܲ�Cq�,�B{|¹Pģ�ŕ�Ӓ�RC,4�����Z��}�S��}�%�H�I��H��r�#�~���ϵXH��AhcP)�~�����4��ą-����]p	�l�8uCp �@L�u�<��旛�۬\k���$Q�]���j$425F/��3� ��Yrd�AG��Jnk&3E�$y$u� +Y&1*~{R���|6�ٺۗ졮�!V��    (pJ��CP�Ktp��X�$͉��<���;��#l�[H�a�5Q]�(Q�
*]1�B��r2l�@�Aj)��A>�s�U-�<�0�JZG�9�.wAޑ�|��&�3�h�K��J�91Ѷ5I<�K�r_�R�QZ��Px0kW���ڃ�v�.�׹��'lz���p3;"�,9��)�`%=�,��cF]�a��I�:3ߧ���C�+�^��Z��,��~bFd�=!K8�!{AK��d}a�Lb��=�Ͷ�vDv���#XD!'x`��!��ں��t�x1֏d| {=���D��r�l'�8K�vVq1�M�tk������.�2�n�h��O��0U�}���IrG\4��W��InӜ>���w�[^�I u��|�pb��ƞ��M���J$�������w�ݶ�"�-RN��.���b�R��h�u+��� ��0�ky��4!G!/�ɱ텸��f�
���I�Jl�8MIo���I{��X3.�u٢J�
�=����	��<���N]f���Z�v�X�Hw�L�n�����x:��0.dꋅ�Ԉ��"�MVYce�+�QD!\N*�Nzr��VűU���xF�!�)�PQ-�ٮ�:�������������|1��DT����5�a&IP�A���ߡ���+���u��&�TiQB�:�<�����蘋}�&����o8s�!��''�o/_��I�3��rQ�܅ �q�����+���1\U�k��cp���V�D���㓖�涋h��Y5�rI��a�˙5ߒ�KWV���T��*%U�lb�D_qݣ��(�L4ų�ץ�Z��'�yRHҎ�V�y�1H.�?�� ��o�I�E�3G�۳p���JTp@�&����2k��P��}��$M�H7�ea<�R�1���κIT��ZB|�C�|,�=6��?Gq�ve͉0����rqg�^`�w�%Uߙ��q�|�� ���a̳�g#�]�����������Qg�#g��۾0�Z���8���S
����=3�0ﶽE�J�굟ʤ_����]D*�Ȗ_rl*�	_���z��12c���\��@���&�dù4��ױ��5W���J��F|ۘ侘�<��-ܮ%+\״B��/��+���H媄��I�Q���Q�n��rR�YhK��8o d��x|�/?��vR��I�I��{�7
����C�d�~�4 ��a��XFџI�m�Q�>�2�p���~OrT�tS>����6'�P������H=֬�r�nm�"�g/��6�DK�J��0,������|Yiw��}�~��������*=g�E�2�,:w5w5�j,:�Z_��D��!znwE��TC��ۣ�ΧR��r�:&
{C��V����$���nEeO4�|)��&TBt�*�NSѹ�~8W�C�T>\��t�z��ٵ�[�psHb�	�u�HpMC�t=C��k:�c����]����/,��s3+:�Za�L���N���j:��ai����ާ�-�9���z��Ǘ]_p=X�;U�b�T5A�O�Slz��j�:��܈j���XŸL����X.ep�u ��Eה����W�T�F$�'������w~'��s�����;�\����p�<�������\C���H��piD�%g�_7�|�Υ��w.+�sY9��������wڹ���y��3s�;W�o�4��g�w.)����Bvzb*�E�G=�xH=��'���gr�W�~x�y�l�]S��S?\��68}.�`w}#���Z[�>�c}w}+\���n�\�9��0lnlml]~��k��[����F���7/}6��ͭ�p�:ף�������>��6������p�Y���},�+��:x�y�������9�B)����./���fg�7������_?P��J�Q����n9��5�?��sEJ�0S��r�'#�g;��q�ke�Q����S+xF�ֻ��Qe�K����:6�)�j��s	�Ή�3����U2��8qfBDU�p|�~89�_�u�k���'�:�v�K��م�yfn���>[NH�{��jv09tD��*�8�c��1�^��F���Ò�ە"�I��2�%oo�8�HYpF	灳Y��Xe(�
r����CNwj�\���s�]۝�H���O��GY�hyΕ���PB0O���H7�R�
U��;���ߨ]"I5����QF4�VI����D,��t>U\ԑp���I��G��A'�_�N�k�C��ߗ�~����T��*s�͉db�f8��y�*��M����jJ�5��t�m�m�E����tG�1��3��
'��hX�"f삃�]��>��oOq�رN�yՑa�%�1ҏ?�:.%�
T[��,\GdtW9�X�	�r���H8P.���Nb����29�bԂ����Sx�㡼 ��O)q6zat�+M4Ǻ�?ԧ�s؞�a1s�e��ڹbl%I�����C��M���1�Z�Xx5���,'�����_Wү�>eӥ���M�]A㓅s�4N��I/hN߫2RSĕ|�6ٸ:�|�e��� ��Lh�]��"�ɍ�aag"&D��)L�҅>���]�W�`��}d�4��܈�CEqA(��gx�-*E�AΑB���#L{�`=xidw�f0�9��;�6�	>���ͱf%¤ �ׇ���I�ubC�V�E���ĕ���l?I:��R�.�1Q��G�����F��m�6��-T��\�vѨ0�u��'�D)kyĒ�>P���4���'d������%+⌫��?��F���rf���	j�k��㼵f�q�6<�sNfE\$���(�SeG�HdĔ�Y�Bf���5(�Y�MN��|� k�l9QZ�Ň��B��6���t�"L��p�:ZB�1D���LC�n�L� ~ٜMpV�.�n�B���SBr��ީ�J�G V�L)N�����#c�a�8����j�\�G(:��ɮs�lXKL��U"C�,yC4���-R6��I�Z�̟7�a����S�gA{_�c"Z���S/c�NU��<BIBqVw���[r�'~C�M+U�� 8!��	�	�s�t60)f\Y�:u�է^qj��0�-����>eZD\�]`�B����f_�84����M����e+��e}'Xj�Hg�(�q!A�U�o`f����@���`Ϭ�(��+%��(g�.��crR`тsh\1Ύ�K�������g�n� T�B!G`�NGȓ.js�Ɛ\���u��Ւ��m��[b�� I�Ҹt�O5�a	�G�KGJ]"���E�8��M����^dH~/� �	Y%�t����=���D=�>����t}��5����	�9��UOIY��Ԉ�'E߉4���	�˦�L�\�V�ވ���A�%��4��$o=�Ґ|`0��;�Qr��Ԭ?&��$���ߔN��.�:ő(ꢊ"�lyg�D~����ߘ�w'R�@��"���U����W&O��h��n�dY�+
��6�ifN��e�5�3&�)�V�3Ւ�MbY�.�#�1��L!�����S��r1�KP��RP~��$WNLu	�.B�O�����u^�Ų����i8]���N�1���nEy`�Wl�9.�N��$�"}2��f�;��Q���+}zHS�A֠H��S����v��*�wi��!����	O��}�1��Hmd]��F�A��_ǒ[���x�WO�7��'iC�s����\��8�Q��d۽㽺}X����5�H�L�+��-�H��̖D�����lC9p���2έ�� p>�}ݠ}�n=����?n/����ME�i����D�e��B�߾��-�����*���Q�"�4��A���R|N��%�EPb!�D����f��}d��2Q�&��]4X����`��3P`W� ;19���A~y.n����O����M5�y�^�Byy��eκt����Ȯ<~U��M}@tyEޏ�2]� $���A���6 �sŇ�[��ny��m�����F�X����7��1��7Q�}<8�E�f�p �  �
��p@�rM`l�A�@�;)��g��X�xWОX�=���Y���sP�J���St:5z��a}{{�tn�pw���ollW�/5��:��[����1���_n:���������V�����j���m�����hbn�6w�>��~5#��HCbg�/�Kl��?F�N�,qހ��ڙ�s�%̽��xb��YD���c=�?X��~�޾̈E��x�E���;�u�]�䕨���_>�������V�>G���w%k'Z�V�1��\�ץ�"���?�:gE�����
W��*�8�`�*���q_��q z��Qe.�����d#���T�oՂ���&�m4���'M��,��C�U#�m�"@�duA���I"5<sފ��[	`�K��ߨw��������p$o��a�ȘB�GGR��,�z��.�	�il���8v���~1Y�������S�"ߧ'P���$8��GZ���>ϕ0�3�]"��v�%��&#�N�t"�}
����%t�.RL�%�w����%�!Pf��2���b]���3��)�|n��2w�=����дsh���R����PWZ���/����;����/Ɇ0
��LMe��c��&[��b��$q�~pJ�Q?�6%=Q9�bn�y(Ĩ2s��٦p{7�
�nL%��
䯐�$���X�T#�dB´]���w=,�S$���O_�1�I@�*E��iZ��-�Jp�=�lg.p�^��HS۵�N��E��:��|�t�7"�4L�f�[����l�e���-�w���m!!���rPz�P~�2��p-��)[F���,�8��ۖ�U�۠����q�pYGv[�����}�s��/]SFB����8ڦ�F�=����S��L�uo?���|�H�@��j�m���ǋ
ݸ���i	�����9��<��X�1�ŎU�%O،���?(�A�}|����|�±5Îh�\4�#]Z	q�z����z�H�FZa5��M^������B�~�v����`��Jɻ����O��g���7�R�
[���ө�fa�N=���4��p��CV��SXD����;����7�5Iώg��s9�����F�����Z� �HpT̀���� ����k�����)����# ���ӺDG��,���w���AG� �����ӁX���)��.9���RbF.�N"�����4Rv�H61*�N�0y��x+4�_0U��,Lm%�����5R�q�F��3�(�>�9���ti�p?/O:��$Ũ����	Ŋ�f����ȺM8%��4��p!8�L٧��w���;���hL��x�����3�;�����w_�^u\9T��e̻�^(���I�s4�F��o���S�]U�l�~y�>�9=���(+��$@���|2X��t�Cm@܈a�+�RH���
0̠Z}��h������"i�0(����m��u1?��j@[�|&彧�6�B��n���Is�\#Be�u�t�ϑ��$G
���a��Q��z��F�r��l�fA)<!��F-��c�ŀeE��8$n�Yė���!H��8����Е�%э��i�[�"���{rڣK<�)9%�L9��wŬ����^\^�l��Sho&���?.�z��_�E�}�߿CQ��v���OH��C�n	�$�\ׅ:ݴh��F��,���Mu��-@�D�8��Ӕ�a���F�ϝX���P� J����`G�$�d���d-� ?:Q�s㰣�o��6�F��ŏƌ�Z���4�pUXQ]t���D�'�i����{]>���;Y}�٢�z����6��`����|�$���=3����Y��L-�Y `X
�������p라���a?��\n^����i?��Z^r����ߢ���va��~����͵��V����y{�����w��o��ͭ�೿��}�����XW      >   �   x����N�0E��W�bƏq�ޥb�	$6HQ +�4���~?IUH�`y}�3gF����!���i;�~s�1g�2P��I^�WJ i��\)9Uh����X�)3D�S�bS��f�B�r"y��t���U�~�oa�U�<,�����8�*}	F9h������6��C������!���	e�u�TzPk�qWKW��Ɓ����)�~a;L�+����	���%�ܒ���^�j@uq�j���Epο�و       6   �   x���M
�0��ur
/���$M��{� ݄v�i+��"xz�N�)��Y���O��Gх�.ۡa�ڶ5�8�V먎�%y��tM�@)�m6�`�k뭑�)@XH��B�5J��߅#����-�/,�ֲ�<Jb�ּ�}Mܰ1I�=� ƁBG��Gep�oQ��2���s� LzD      F   �   x��ϽN�0���~
�@�;�����:!�,DM�1��I�H +醓���ld%OE�ǃx��q��R�?6������]��Oふ�e�o@5hR4�D
���WWF�
Q�$�dB-͐f�ԧ�����>*�I�1h[K�}w9+���<���<tC�._y=�M:��9���@(AG ��M��5����?�D�$�O�{�i����F�7�:WK7.�8Z��hQE���\+9�_��R      D      x������ � �      K   �   x���;�0E��^����{-4(D�
��)i�,�ޫ�F'����+-���z�y\�YM@�0p,�� �8W�9@wQ�c��$w�\<G�L����S�T�˟��Q�^�xs�����q���c������L�����      @      x��\�r�8��[~
$�nv�֤�� �L�����;ٯj�� 
�8�H}$e��׾ƾ�>�v���V|(3ɮ��	4�F?4��o�yQfi�|b~���ߊr>���ƽx���������KA{̣��1�1¼���s�^�{1{�}�d^����<�YN	V!�,':%����I#C�i_xxC�rb�Cr=1���������yn�,�i��C2��8�iI��D_aU�#xrH�l8L�*��bE4�2�����?~uN����<)'K����	��d'��븜 �t���#]�Y�R�����C�|�̋Z�
����H%�82P��o���1��7�����lxH�y�^§b�����Ԥ�	eW0�b�0>2����)��fɗ�$f�C'ӱ������ ������\4|�nl��Ρ!,���d���qD��72r��C2����Z���$�q.�G��t �"c���ö>�@?��tI#XƤ�g7�lir���<'S�J]`�$���X'm��
�S��'�vX�1 Sqb���A���u�.mM�D��pe�9pE T$�`*�h�s�Z�ۋ���HOc�*�0����?���T��OM�߸��8��,O�(� �k���<K��9p�_�#8ر�x�����oO^�^�����7�ZU��pU@J`X�V����a߳�4*��P+g����fd����r��q;�D����[&?ٚﱦ�Q��4ő�\�߶92�e݉ÅF��5ˑ3��x��(<-�)�~Z���F��$���{rg����]Ŕ�*�����4`<�T�z/�����x��GSM�� �I]�|�q�B���#���IzQ�y��_��[rvZ��廳����ዣ��Z�IN�^nP�K*�L
���㲄Y2� ��:��F3��'�7Ou�K]�A����V�))�蛧�������;��1��s� ��^n�M�l5zT=��Qti�1�#v���
4�`�h�py��o�^��r����>>A��*}��:���OpY�ނ������@M���v
������1�<Lo-�o�lJ��Jr
� ��4N��yBN�i-�w��{��K�I]������,�N�ʽ�|��>]��[��`�QG> OS`V��:_9"gk���m�%�;x��@c��.�juv��Ȕ7}�-;[�����[�x�srR@�¶+�1���!�'��Ck4�8���<�)�����5�C�Z�����ঘd�&/\�i��=�SWϵ�(��#v lj~�.���q��'tgi���/ YLt�/���S������q����<� s=�(�%A�K��	*��A�&�tN����<�p��s��|�<���֍�>mә��Az�?���������^���$Jya���)u��$.cS�V��ޡ��5�4P!W��lH�s�K��N
�W!��w<G|�{;OP
'��Ɖ��,1�����(�r>_]�=�v$T�(泗�T~��ea0Ɋr� �9����e� ��a�k0��u�����{>�Z�)�#k�xY�|HҬ����26��*�-W �	?������
�?����W�9���lU΢K��#�iA�zj����+/7_y��I6��T6��J�.�l+H��aEH��H]Z�? ɴ��i�
[?��-��c��
 9�<@�z��S8]�!N���1�
��<��17�,A#�Fn���fA�7,лA3X���L� �A��ppi�j�UKԼ*=X�K�|�)����s�y���ޯ?�V|�ݱr�w�fl������?���"�[k������t*/�'�h���[�1\z8�V��*����B����g"����\�. PC���.u۩8�YWz. �W��q�W(?�>���I��M.@7� "��_`��y�\[��&�B@��˅؀���X��qPe�ڨ�I����b�A�׸YQXg�d>�B-�+��8��9��0��t��쇣Kg����*KZ�P��2�.�¡�`�)��#�/3j��WXQc�Z�yTMԉ����xX�e���Ė��8��@ۨi�Ƥ�ܿ��cm�����sm�����Vq��)H��Des\	K����H��%���\���h"����D
��?�|���������?6�AP(���Q����K�v��8��xc�`��k�   m;$ xKP�S���2u�H�:�P�K���m8Z:��BTN-2%���m�k2���.�,��� Q]c#Ci�u�B(�r���unRJ�IA2Jq {gqY�y>��@|�7N�A����6�y�Q���	�S�	ю�ǀ��ѡt���+���LLtI~�}I]`d�/Q�A��K��Y�B/���*o����/`���Q�l�M�^�Ӳ�u��,6��jihJ'E۝�\-����
0+��拁W�%(Y�j��}��������x��Dr+�xņ��娯vH8����$��۶���t�WT�幙ק��Ӑ��w�W=�Յq%T����_�qY��>� ��n������S�˼�㣴/���`Ӻ��~�e�N��NW����U" ���. .��@*����ҽ/��|ieATY���U5n�� �R?x�>�
A?T���͙�@��]�\B����J��P_��J�n�}W ���)*�3PƂP��w�8!o�,����=�qБ8(TN�o�Q���=F+ۮw����㢿I���̖��Q�we�>'Փ����Y�<sr�RO��n����DFYZ:E���A����$N�SgcQO�sW�*��?�I<��C�%�������Yi���]޿j�*cl�$�yIN煙O��wל�v�y7�m��6�|���b���\��H6f��0�k��H��f`D�9��Ҏ15�Nț���t����2�¦K���
Km��&V�SDC�#N�)?�|�A Xx�z+�"]uf=5y��v��8�c�Ry�Y��p����Ln�c놁�"/Z��u�_V�n���0��D�8�b�(�\`��"�[V�)of1�&q2����2	b���k��?V�@��Rd�]��+7
���3`N��Ħ {M~Φ:�6�}�	q�;��mEM��e	W>�ɖi�.�w�=�?a�{HO��]r�%��	oHw��]J���I8�j��REe���k�#�����ߥ��9���XV����B�YA�_�����e�sc��ϮXHǊ�kV�$�[~�ܨ&yG+�-f�ޫӵ��m{�5�lg#	��F������G�B(�m�y_�L���T�/A���nٻE����k���d�a'ux�E��.��U�����:@*FF�9&��'qiމ�<�I�}�������XI��B�x|e�����	x��b���s��c��A�&��;��F��b<���7[�9Ex �i��^��f��{��C�Y�t����sXqQ�ʹȍ�SNƞJ��,C )�+m��Ǚ�N^W��!�4,S��Ŕ���^� �{(ϝXל�hb �"��Pƒ�ë7��5�S�f�~,�6���ű�K���!�UA���b��9M��hB"�v܈�4�~H�lKL�������ϰ��"��kksߡ+�Ĺ��	�@ �g��7�+�tH�K�d��b�W��+sZ�	?P>��5�����}�]�h!��Mm���'7�#J%��Cz�s��bk�!�|%7WAK��,��Y?����c��|7d�m&ƺ[n��ٰ:�ā����e���7�y�C��Zz�
�i�V�ں����A��������ggx_.C&�VF�rW��Z���41�95`�`]kZ�k��e��gL��$�O�{��=��K�\����L谕M��:�G�#^��Yi�J�3�^_���2Z�]�+�����`�m�7w'�KD�À���Юb���A��6���A�4L����B����I��n?�|��Q��%?�dH}o���R۝�Ipߝ$B�3#&��
dh��h)4�*P�Gc��:�,�*��H`p�|��j�c�W z  �Ϙ#�U�����01������8��}v�.�t��+}�_����_���	�x��O���=��GoO��_]���ۛs��]�_�j��Ұgw��,��y��`�L��c]��4�&V�,��vTy��8�$��"�D%�w�� ���|6���f�ݳ8���G�<����%���id�������K^X��mrc�8����3��x� 7]0��f)��Z�87�v��l������/��¯-E��񥶾���w�LQn(B�T�BE�	�+|*B�x�Wh"_������{�ROg�n`_����Q���:\u�P�ֻN�IS����K@�酞T��[�S"
p�6�3 �~�0�����{�N���#�;�������V!;�zw��ا�TŚ�-6���F��#x��Ц<�^�F��&�MbM��Z[�HIi������P�pa���Ԗ�R�sh�r��'���@���a*����P�=GI�y�GY�|����cS���Q������g��u���r�^�6��	�M$K@�k���QU��[�~~ U�z�޶��Q�㝛ܥ�vVy�F��υp���M<����ۋ;�|��)�b���^Skf�AG����<$g� 9)��Lm[c��e�~k7��K*���"�������s�5�G��؋s �i�'���Z �����&��� |gK�.���?.�5@��T�\� �S
���zB5q3��{���{���_4�^E0h��ط�Լ�ٟ
k�r��m����í��{S���6x|o������5���)��� Iw;i<��z�h_JWR.�p��wa�
iCjǒà�nؠ�З!�z[<!~u���\�      I   �   x�-���0Ϧ�����%�ב}8Xi��E�H�p���6����7��Bg�?�E4q�����B'W������ɞ,ʦ��n�Hi#m����6�ɋ-���)��ʓ��k����>L��a�|�Sl�x�*�����&q      B   @
  x��]]���}����Ѥ/���$9����vI��F�HI�J���������\�b���y�J֡8w�=s琫��ޮ]M�.�w���yM�k�SW�>_d�����7���g/w��g4�����C�н�kZt�i�|��ƻ2��׬���|ὧ��^����ȗ���sO]Y4p�������C{�Ε>�! F�S4ߔկM��fC�x�z�i��e��Y}���0]���m������B�]>?�^,}UeE�6��}UӢ�I�[4�^^��o�*� 0FE��.����/謄��qp�:�g;W%9������vZ���1×8�=_�0�%���7�s��)������P1z����1qY��������i��	�Z� �#����fQ�Xˢ�'-_�b1�0m��$p������9[m�|u[g[o�|;�6�� d��j�naN����w���x&�Hn����J�r�jΔN�B��[U�r�v�1��|�j]�����ڑ�K��)+���5���#'gU�`��G6��������[b"@`�
�E}]�(�S�|�>��
���O����2 �e������w?�1|�qg5��m|Gj���8[`�����y����\������nEg��]}�Aݞ�MdC�&a����QP*�����0V ���Aa�7�T�l�C��ƹ�xX(8��ŸM2�����F

H�F'��mf�6��3�Ǻ�u�x��*�'�r��YQ��u��X�a�ͭ[AX�u�Y�ѠzxS��}yRv9����&�ǌ����=q�oS����ڇO~)`I7�f����r��K�[4����p���&���a����AbI���c�P����T%�E{�x�r[���X��3�C���4s����4�V�B5�a�I��K�f�W�e�S-Y�y2���ož��Tlw.?��mKa;ز�[��E�AVX�.�r(��C���4��\d��Q�n�8��H5�jܮq�Y��ѿǯ�f�>-я��w�tt]����ޥ/^l7����o�n�`"/N����o��9��&���g ��篡��j�(�/_����j�-м�GB��
����玮`��e6U_M*bY��ũ0�X)uLL[a�Z�ʇ���\��z��3�w��ґ�mP����5%�Gm>�u٦.R�=����.�,U1�`�FҾ���/��_A\_K���66xlO
�u?���1~�~x���`��y�r�Au(1�m�3�k���yQ��������~48ukur�6QX�z�Ri�ZYه�T�L�6�D��a��㦙W�v��r[A�k�j�Sݷ�b(�����=TWFζ;(���; =<8�w��{W��'�)%<����d�
ɽ9��'g5s�{��`U�Z��]�g��+x�8\�/�.D��[�o��o��cmA��j�c3�$8׫8ב|��Y�jE!6���[/�־����ic�& "x��O����� �D@������~��.S��P��i"B{4�C
#��ZIM@D��^�{��{��^�
��$U�	i#5��}��}�nS����j9a�����ѕ�H'6b""
��7_G�7�q�TO@D���~�{}̺I9G��x""	��_o˕6�'��)�lp��v��'A�\�$�"I& "yp�Wq�#	Q�)k��"8��`/;�]{���������>���OR!Y��G���:x)?A�E�d"�d
"��g_��W%L%Z�>$9�T��Jd��l��?&`�8����]��(8ث8ؑ�(��d�w�]��88��`/;�]��ie#�߆��Ip�Ow��#�XLG"8�.D������Z2	�ov!�x�n��S���q��ru!�Dp���^��>��P<56D\����
�[�q�#	��j��݃}�����p�]��[f�QBM@D��/�`G�׆�D#' �B���(�	BEC�;Q����`4����$�؁�
]\��^���=	�c��U�w�]����u;H��[2Q"��\t!�B'Wp�vDWF�XX�w�.Dt���v���_�X-Mҿ~�BD�.�'8��*�1K������]\];B��7�s3:������Mz4:U�)Ε����\�q��~�Hhne��BD�N��`'숮T�YqݿT�BD�N�/�`�Gh�����x"��vr�`a/[��U�b�:��˶.D���r�߃���B�X�J/#Dr'6X�ϵ���+I�"�b;�[�h��aGhW	�ֶ5܅����E��&����e\(��P�BĄf�/`b�[������@Ą.�'x��M�8~H����-t�����耔LZ�k�z�z�������E+A�mb�DLh��7�d�íFM@Ąn�`a���` ,���EIdM2ڹ��aG�׆%\�����1���)�f0�]��<�RځH���v�C��4ɣ�?�H��������iP	��Q2��M����z�:ȈL��/��_t��y���N�ׁ�T�#��P����X��D�:B�������B$���>��hש�L%FE}�ՅH��������<Q�y�!�h�w�      Y   �   x���1�0������k�i\�����h�0"���+0����{Ϫ�.���^������z���L�:�ݯ�kڡVM���r�Гg�,����9y0�oQq�ZB�UL�H��n��Ȃ��� P ����̜�0+��W'�y�P�Fk��?_s      S   �   x���;�0k�\ ����GQNA�kIDb+�� �!h�kF3�Ӭ+�1��R'K?l��h���|_�b.Ħ�DI�%3�0P�7
�`�>Z+��6؟Ttإ����]P�tMb��oA���?��_z�y�k>�.�����F��+��s� ��jt      :      x������ � �      Q   u   x�m�A� ��p
/ 2_$Ɠ��J&��k��}���c�]�\�O=�ل�3I�ϖ�Fx��-4Ё#��l&7hi��w��%I�.}[�k���A;��~6b6��NmF)�I�$�      H   M
  x��Xێ۸��<{3� #�N��H&��"���L��Ȣ����\�;���Л>D�&�$���A��m�-
� ��I~���#��UR<�Y��])��=k�r�MSO���ۭW�E���rs�<�_��]*���o�"͚�%��Y)�E�bM�XI�A؜H�U"*��7���k��YsV6���L�o
�*wd�KQ��#^¡  �9i$a�KAiWo9ox�d��b+�=�Z�FT��P�⎭��Qr�{��]��J�ּ*8ٰ;N:�!��R���QU��Us��k��	��j��[�J[���p�6'jdY�{��X�(ۅ�0߂ID�(�h���V\�������V4k4-��<(�T�h:�<�^n�Q|n#�9'��l�Q�۴]˒k�����:�t�Yr^�����m���2�BA�lD)��'��2�!��8�
VJ�WSB�l�䠩!�*[X��_�/>�OU��������8��u�
�?��w�촻�lY��(��F��!�\���m����Ҽ\^*�����s��w
L�&Z�k���CV�:�ҩzM�b�`h�bɭ�D��������
����ʜs̅�����fE��R�onYՀۚ����ȷ��ܸ��y+��̀ȥ���Ƭ����gֆH�}�.��#S^�
���nΕڑ۶h7���#�CzC
� $���F!�(q���kQ�W�LK*��/!Hr{�em4��]=�R��tN�K�Y���Be*���[�R6��\�H����R5pD����0�[x��-�+���6�C\@�C��"x�ƨ
{�9߼xS}�����<���N�����R���O��e����w���=�r���N�����j�l�Ƨ(���	� s��C���N���(M�(��(M���8�h����'�&W�[5�c�V�]���j�q������KH�Wl�#�5F��.B�V���o�B����c��w��I�2��\�6m�.������l�]]r^�b������B��e�э
l/PBߙ`������҂3�n��CQm��B,8V ���R�+�?�z�{�j�d[-8��qQ�VCā��
�CMʫ{�d����	l�A�gPBF�]3�)�¾��m*�Ҵ���1u�@���*^2�z������o��(E�� ҡ^I�f[0��CkW
s�QC���9��NG�`�{a��9c^rb��<tMh�`6����{!L 0��y����d��{p A��zɵ� �q]+[,��i��U���u�V� ���3l�L}�LmXc�@���8 ���Zt֘�f*9����y�{+u�h�G����>��0I�A�=g��{���)��X�������`fΞ�5��P�{����,_��Ä��.V�~��ψ�J�����$���`B���Y����NB
�4�����ħ^DI�c�/}�T䢥a�%Ж�^������	��)�͡�B:�4,�$�?0����څ�h^x½x�]C]q��Rw�Rs�p�%��Z	�I�N����8�!�^P?�u���.�s#W!Z��^a�Z�Q�����	� u�j�'D��hP�6�� x�P�!M��:���An���; ���A8.c^�E�c���rQ��˝,�@i�IE�^��4�(��,���)�S%ul�����f�dsX-��&G)׊W��4��D�O/�D�������P��a�d}O�N���8����7��u��e\�=<�2N������'���4�G������PG8w^B�0!�����!/�Ɉ=�ސ/?�>�{�ɍ�
4�a�M�l���Q�����)��!�W0^�����+ѰWI��
e����c�!�����|��^��!������SО ��v7�&\
|re����Bd�͖Q���}SC{�i?���Q(�#�:�������ڌ0v�~�GX/���3�)�
'Qe&�i6E^@�0��:2��7���l+6dV@�(�9" m�1��rQ̵r�DOt�}?����#��<��~pX��Y��:��Z�Δ������豗�ڃ�k���(�kN��)�Y?-�ddQ�(�h:�S�H��A�����?`��a%��O��bi�̝��龅'����f8@��qe4���)�H<qnۆ���m�vo���i�ʇY84]��6�*{T��_"Ϡ#��y͗�[�3�e����Y���#����}c�N�A�P�d�<���ByJ��o�3��9s`F�����v4�)�����=�ǧ��� �
�4{��h���Бq�L᛹]p÷��0<��	�P�q��~��>��B�����@Y���@�S��;��A��Nfi6���fQ��q@ȵR�Y9-�#}&� ��f�q�#�'�BGeg*���r��/�RmV�q�L�uзbH��4�h&�d�PJ� ��:�v*�����I6�)?��|
iԯ�=��:�� |3�^�BΏCq�>�~T(�Fzs3��ď:B
�,̒$~
:R��կD��n��>~��$�}P���2�3[��9��5�<2���1�w�`���~���M/��<�ǹ��=G��8gЁ�'����3�x�      <   M  x��S�n�0<�_���'���𫂁�q�ql�j�Zb%�)�t�k�-��R��E�S
�P@����쐳����t\��;S�}�'�&��j�2&�+�f�X
\���w�L.Xс����ϰT�l2O��ܙ.�xJaEUӘAI3��2t`�Tf�,hIm���S�n[��.E�\�%�<���j��ʪs������;�eEaJ��S"R|���n;��)Z���G�����S�ۜ��8oipN3���G#�2�ԁ�b��"9��FX�@Ӕ���E�$�J�r
��
f4z+��z�j�Q�o.����j�)X}��\Ϡ���ޛa|]�U��{Y*����mڹ1���~���*����Ϻ5�Op!�f(5i{�a���[�����������j��1*čkvUJI���u�=��Kz�.q������y�U]��مLN
�QΙ����H��!.������y��R�l53)���f:W�d�ub���]A��n�d��?y����`���k�P�������;��� ���e�׹4�,�&��Q0�W�?8��{�>.q-��ڠV��]��      W   �   x���A�@E�3��S�2�)gqCp�t����4q�����>9,�g���p��}<�0-��ko���l.rRI��R@\;�."4���Q0$�&�Q�Z�G�&ظ˺.]Um�V���r������IF"%��FAZ���u{      1   �   x�U��C1�{����Oإ��Q�*���B6�-I׽8hr�bb�,�?O*�d2U�D3,�'s��o��Z�:�{�*5��k�:Lf�<)��n7��?��t�Y��n�����d�f�/�!74���Tŕ���n�=�f��
J�|,����Z� H]|      4   �   x�m�Ak�@�ϛ_�!W��ٙ�B#�֋ ��m
�6!�mL��7�T�=��񽇦�����Ԧ��59�1���a�G�j�l�C����sx�(=�󏔏��eU�|�˝���2���Ȣ�[���a�"#�'�`��-�,G�h	��}�@�%�� r�T�Ї���	�(�+$����Y��yz:�     