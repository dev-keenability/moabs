Rails.application.routes.draw do
  # require "admin_constraint"
  # mount Ckeditor::Engine => "/ckeditor", constraints: AdminConstraint.new
  mount Ckeditor::Engine => "/ckeditor"
  
  
  devise_scope :user do
    get "/users/sign_up",  :to => "devise/sessions#new"
  end
  
  # devise_for :users
  devise_for :users, controlllers: {registrations: "registrations"}

  # devise_scope :user do 
  #   get "/admin" => "devise/sessions#new" 

  root 'pages#home'
  # match '/our-events' =>               'events#our_events',             via: ['get'],          :as => 'our_events'
  # match '/events/houston' =>               'events#houston',             via: ['get'],          :as => 'houston'
  # match '/events/austin' =>               'events#austin',             via: ['get'],          :as => 'austin'
  # match '/events/orlando' =>               'events#orlando',             via: ['get'],          :as => 'orlando'
  # match '/events/atlanta' =>               'events#atlanta',             via: ['get'],          :as => 'atlanta'
  # match '/events/cincinnati' =>               'events#cincinnati',             via: ['get'],          :as => 'cincinnati'
  # match '/events/dc' =>               'events#dc',             via: ['get'],          :as => 'dc'
  # match '/events/philadelphia' =>               'events#philadelphia',             via: ['get'],          :as => 'philadelphia'
  # match '/events/pittsburgh' =>               'events#pittsburgh',             via: ['get'],          :as => 'pittsburgh'
  # match '/events/salt-lake-city' =>               'events#saltlakecity',             via: ['get'],          :as => 'saltlakecity'
  # match '/events/south-florida' =>               'events#southflorida',             via: ['get'],          :as => 'southflorida'
  # match '/events/tampa-bay' =>               'events#tampabay',             via: ['get'],          :as => 'tampabay'

  match '/about-moabs' =>               'pages#about_moabs',             via: ['get'],          :as => 'pages_about_moabs'
  match '/about-amy' =>                 'pages#about_amy',               via: ['get'],          :as => 'pages_about_amy'
  match '/partner-with-us' =>                      'pages#partner_with_us',         via: ['get'],          :as => 'pages_partner_with_us'

  match "/contacts"              =>        "contacts#create",              via: [:post]

  resources :users, except: [:create, :update]
  post 'create_user' => 'users#create', as: :create_user
  patch 'update_user/:id' => 'users#update', as: :update_user


  #########################################
  #==blogs url for blog model
  #########################################
    get "/blogs", to: "blogs#index", as: :blogs
    get "/blogs/categories", to: "blogs#index"
    get "blogs/:title_for_slug", to: "blogs#show", as: "blog"

    get "blogs/categories/:category_slug", to: "blogs#index_category", as: "index_category"

  #########################################
  #==press url for press model
  #########################################
    # get "/press", to: "press#index", as: :press_index
    get "/press", to: "press#press_page", as: :press_page
    get "press/:title_for_slug", to: "press#show", as: "press"

  #########################################
  #==events url for events model
  #########################################
    get "/events", to: "events#index", as: :events
    get "events/:title_for_slug", to: "events#show", as: "event"
  
  #########################################
  #==partners url for partner model
  #########################################
    # get "/featured-partners", to: "partners#featured_partners"

    get "/partners", to: "partners#index", as: :partners
    get "/partners/:title_for_slug", to: "partners#show", as: :partner


    # get "/partners/la-croix", to: "partners#la_croix", as: :la_croix
    # get "/partners/cribs-for-kids", to: "partners#cribs_for_kids", as: :cribs_for_kids

    # get "partners/:title_for_slug", to: "partners#show", as: "partner"





  # match '/events'              =>        'events#index',              via: [:get],            :as => 'events'
  # match '/events/show'              =>        'events#show',              via: [:get],            :as => 'event'






  ############## ADMIN SECTION ############

  namespace :admin do
      match '/' =>                                'pages#admin',             via: ['get'],          :as => 'pages_admin'

      match '/pages/home' =>                      'pages#home',             via: ['get'],          :as => 'pages_home'
      match '/pages/home/update' =>                      'pages#home_update',             via: ['patch'],          :as => 'pages_home_update'
      match '/pages/home/delete_home_page_bottom_ad_one' =>                      'pages#delete_home_page_bottom_ad_one',             via: ['delete'],          :as => 'delete_home_page_bottom_ad_one'
      match '/pages/home/delete_home_page_bottom_ad_two' =>                      'pages#delete_home_page_bottom_ad_two',             via: ['delete'],          :as => 'delete_home_page_bottom_ad_two'
      match '/pages/home/delete_home_page_bottom_ad_three' =>                      'pages#delete_home_page_bottom_ad_three',             via: ['delete'],          :as => 'delete_home_page_bottom_ad_three'


      match '/pages/about-moabs' =>               'pages#about_moabs',             via: ['get'],          :as => 'pages_about_moabs'
      match '/pages/about-moabs/update' =>               'pages#about_moabs_update',             via: ['patch'],          :as => 'pages_about_moabs_update'


      match '/pages/about-amy' =>                 'pages#about_amy',             via: ['get'],          :as => 'pages_about_amy'
      match '/pages/about-amy/update' =>                 'pages#about_amy_update',             via: ['patch'],          :as => 'pages_about_amy_update'
      match '/pages/about-amy/delete_about_amy_bottom_ad_one' =>                      'pages#delete_about_amy_bottom_ad_one',             via: ['delete'],          :as => 'delete_about_amy_bottom_ad_one'
      match '/pages/about-amy/delete_about_amy_bottom_ad_two' =>                      'pages#delete_about_amy_bottom_ad_two',             via: ['delete'],          :as => 'delete_about_amy_bottom_ad_two'
      match '/pages/about-amy/delete_about_amy_bottom_ad_three' =>                      'pages#delete_about_amy_bottom_ad_three',             via: ['delete'],          :as => 'delete_about_amy_bottom_ad_three'



      match '/pages/partner-with-us' =>                      'pages#partner_with_us',             via: ['get'],          :as => 'pages_partner_with_us'
      match '/pages/partner-with-us/update' =>                      'pages#partner_with_us_update',             via: ['patch'],          :as => 'pages_partner_with_us_update'


      match '/event_index_images' =>                                       'events#edit_images',             via: ['get'],          :as => 'event_index_images'
      match '/event_index_images/update' =>                                'events#update_images',             via: ['patch'],          :as => 'event_index_images_update'


      match 'event_index_images/delete_event_index_bottom_ad_one'               => 'events#delete_event_index_bottom_ad_one',        :via => [:delete],        :as => :delete_event_index_bottom_ad_one
      match 'event_index_images/delete_event_index_bottom_ad_two'               => 'events#delete_event_index_bottom_ad_two',        :via => [:delete],        :as => :delete_event_index_bottom_ad_two
      match 'event_index_images/delete_event_index_bottom_ad_three'               => 'events#delete_event_index_bottom_ad_three',        :via => [:delete],        :as => :delete_event_index_bottom_ad_three
      match 'event_index_images/delete_event_index_side_ad_one'                     => 'events#delete_event_index_side_ad_one',              :via => [:delete],        :as => :delete_event_index_side_ad_one
      match 'event_index_images/delete_event_index_side_ad_two'                     => 'events#delete_event_index_side_ad_two',              :via => [:delete],        :as => :delete_event_index_side_ad_two





      match '/blog_index_images' =>                                       'blogs#edit_images',             via: ['get'],          :as => 'blog_index_images'
      match '/blog_index_images/update' =>                                'blogs#update_images',             via: ['patch'],          :as => 'blog_index_images_update'

      match 'blog_index_images/delete_blogs_index_bottom_ad_one'               => 'blogs#delete_blogs_index_bottom_ad_one',        :via => [:delete],        :as => :delete_blogs_index_bottom_ad_one
      match 'blog_index_images/delete_blogs_index_bottom_ad_two'               => 'blogs#delete_blogs_index_bottom_ad_two',        :via => [:delete],        :as => :delete_blogs_index_bottom_ad_two
      match 'blog_index_images/delete_blogs_index_bottom_ad_three'               => 'blogs#delete_blogs_index_bottom_ad_three',        :via => [:delete],        :as => :delete_blogs_index_bottom_ad_three
      match 'blog_index_images/delete_blogs_index_side_ad'                     => 'blogs#delete_blogs_index_side_ad',              :via => [:delete],        :as => :delete_blogs_index_side_ad

      match '/press_index_images' =>                                       'press#edit_images',             via: ['get'],          :as => 'press_index_images'
      match '/press_index_images/update' =>                                'press#update_images',             via: ['patch'],          :as => 'press_index_images_update'

      match '/edit_press_page' =>                                       'press#edit_press_page',             via: ['get'],          :as => 'edit_press_page'
      match '/update_press_page' =>                                'press#update_press_page',             via: ['patch'],          :as => 'update_press_page'


      match 'press_page/images'                   => 'press#press_images',               :via => [:get],           :as => :press_images
      match 'press_page/images/create'            => 'press#press_image_create',        :via => [:post],    :as => :press_image_create
      match 'press_page/images/sort'                   => 'press#press_image_sort',          :via => [:patch],          :as => :press_image_sort
      match 'press_page/images/:img_id'               => 'press#press_image_delete',        :via => [:delete],        :as => :press_image_delete
      
      
      get "/blogs", to: "blogs#index", as: :blogs
      post "blogs", to: "blogs#create"
      get "/blogs/new", to: "blogs#new", as: :new_blog
      get "blogs/:title_for_slug/edit", to: "blogs#edit", as: "edit_blog"
      patch "blogs/:title_for_slug", to: "blogs#update", as: "update_blog"
      delete "blogs/:title_for_slug", to: "blogs#destroy", as: "destroy_blog"

      match 'blogs/:title_for_slug/delete_blogs_bottom_ad_one'               => 'blogs#delete_blogs_bottom_ad_one',        :via => [:delete],        :as => :delete_blogs_bottom_ad_one
      match 'blogs/:title_for_slug/delete_blogs_bottom_ad_two'               => 'blogs#delete_blogs_bottom_ad_two',        :via => [:delete],        :as => :delete_blogs_bottom_ad_two
      match 'blogs/:title_for_slug/delete_blogs_bottom_ad_three'               => 'blogs#delete_blogs_bottom_ad_three',        :via => [:delete],        :as => :delete_blogs_bottom_ad_three
      match 'blogs/:title_for_slug/delete_blogs_side_ad'               => 'blogs#delete_blogs_side_ad',        :via => [:delete],        :as => :delete_blogs_side_ad


      ############################################################################### 
      ## Events
      ############################################################################### 
      get "/events", to: "events#index", as: :events
      post "events", to: "events#create"
      get "/events/new", to: "events#new", as: :new_event

      get "/events/images", to: "events#images", as: :event_images

      get "events/:title_for_slug/edit", to: "events#edit", as: "edit_event"
      patch "events/:title_for_slug", to: "events#update", as: "update_event"
      delete "events/:title_for_slug", to: "events#destroy", as: "destroy_event"
      
      match 'events/:title_for_slug/images'                   => 'events#event_image',               :via => [:get],           :as => :event_image
      match 'events/:title_for_slug/images/create'            => 'events#event_image_create',        :via => [:post],    :as => :event_image_create
      match 'events/:title_for_slug/images/sort'                   => 'events#event_image_sort',          :via => [:patch],          :as => :event_image_sort
      match 'events/:title_for_slug/images/:img_id'               => 'events#event_image_delete',        :via => [:delete],        :as => :event_image_delete
      
      match 'events/:title_for_slug/delete_ad_one_image'               => 'events#delete_ad_one_image',        :via => [:delete],        :as => :delete_ad_one_image
      match 'events/:title_for_slug/delete_ad_two_image'               => 'events#delete_ad_two_image',        :via => [:delete],        :as => :delete_ad_two_image
      match 'events/:title_for_slug/delete_ad_three_image'               => 'events#delete_ad_three_image',        :via => [:delete],        :as => :delete_ad_three_image
      match 'events/:title_for_slug/delete_side_ad_one_image'               => 'events#delete_side_ad_one_image',        :via => [:delete],        :as => :delete_side_ad_one_image



      ############################################################################### 
      ## Categories
      ############################################################################### 
      get "/categories", to: "categories#index", as: :categories
      post "categories", to: "categories#create"
      get "/categories/new", to: "categories#new", as: :new_category
      get "categories/:category_slug/edit", to: "categories#edit", as: "edit_category"
      patch "categories/:category_slug", to: "categories#update", as: "update_category"
      delete "categories/:category_slug", to: "categories#destroy", as: "destroy_category"



      ############################################################################### 
      ## Press
      ############################################################################### 
      get "/press", to: "press#index", as: :press_index
      post "press", to: "press#create"
      get "/press/new", to: "press#new", as: :new_press
      get "press/:title_for_slug/edit", to: "press#edit", as: "edit_press"
      patch "press/:title_for_slug", to: "press#update", as: "update_press"
      delete "press/:title_for_slug", to: "press#destroy", as: "destroy_press"


      ############################################################################### 
      ## Partners
      ############################################################################### 
      get "/partners", to: "partners#index", as: :partners
      post "partners", to: "partners#create"
      get "/partners/new", to: "partners#new", as: :new_partners

      match '/partners/images' => 'partners#images', :via => [:get], as: :partner_images

      get "partners/:title_for_slug/edit", to: "partners#edit", as: "edit_partners"
      patch "partners/:title_for_slug", to: "partners#update", as: "update_partners"
      delete "partners/:title_for_slug", to: "partners#destroy", as: "destroy_partners"
      
      match 'partners/:title_for_slug/images'                   => 'partners#partner_image',               :via => [:get],           :as => :partner_image
      match 'partners/:title_for_slug/images/create'            => 'partners#partner_image_create',        :via => [:post],    :as => :partner_image_create
      match 'partners/:title_for_slug/images/sort'                   => 'partners#partner_image_sort',          :via => [:patch],          :as => :partner_image_sort
      match 'partners/:title_for_slug/images/:img_id'               => 'partners#partner_image_delete',        :via => [:delete],        :as => :partner_image_delete




      ############################################################################### 
      ## home page image
      ############################################################################### 
      match '/pages/images'                            => 'pages#images',                   :via => [:get],           as: :pages_images
      match 'pages/home_page_images'                   => 'pages#home_page_images',               :via => [:get],           :as => :home_page_image
      match 'pages/home_page_images/create'            => 'pages#home_page_image_create',        :via => [:post],    :as => :home_page_image_create
      match 'pages/home_page_images/sort'              => 'pages#home_page_image_sort',          :via => [:patch],          :as => :home_page_image_sort
      match 'pages/home_page_images/:img_id'           => 'pages#home_page_image_delete',        :via => [:delete],        :as => :home_page_image_delete


      ############################################################################### 
      ## about amy image
      ############################################################################### 
      match 'pages/about_amy_images'                   => 'pages#about_amy_images',               :via => [:get],           :as => :about_amy_image
      match 'pages/about_amy_images/create'            => 'pages#about_amy_image_create',        :via => [:post],    :as => :about_amy_image_create
      match 'pages/about_amy_images/sort'              => 'pages#about_amy_image_sort',          :via => [:patch],          :as => :about_amy_image_sort
      match 'pages/about_amy_images/:img_id'           => 'pages#about_amy_image_delete',        :via => [:delete],        :as => :about_amy_image_delete


      ############################################################################### 
      ## about moabs image
      ############################################################################### 
      match 'pages/about_moab_images'                   => 'pages#about_moab_images',               :via => [:get],           :as => :about_moab_image
      match 'pages/about_moab_images/create'            => 'pages#about_moab_image_create',        :via => [:post],    :as => :about_moab_image_create
      match 'pages/about_moab_images/sort'              => 'pages#about_moab_image_sort',          :via => [:patch],          :as => :about_moab_image_sort
      match 'pages/about_moab_images/:img_id'           => 'pages#about_moab_image_delete',        :via => [:delete],        :as => :about_moab_image_delete


      ############################################################################### 
      ## home page image
      ############################################################################### 
      match 'pages/partner_with_u_images'                   => 'pages#partner_with_u_images',               :via => [:get],           :as => :partner_with_u_image
      match 'pages/partner_with_u_images/create'            => 'pages#partner_with_u_image_create',        :via => [:post],    :as => :partner_with_u_image_create
      match 'pages/partner_with_u_images/sort'              => 'pages#partner_with_u_image_sort',          :via => [:patch],          :as => :partner_with_u_image_sort
      match 'pages/partner_with_u_images/:img_id'           => 'pages#partner_with_u_image_delete',        :via => [:delete],        :as => :partner_with_u_image_delete








      resources :events,                 only: [:index, :new, :edit, :create, :update, :destroy]
      resources :faqs,                 only: [:index, :new, :edit, :create, :update, :destroy]
      resources :images,                 only: [:index, :new, :edit, :create, :update, :destroy]
      # resources :event_images,                 only: [:index, :new, :edit, :create, :update, :destroy]
      # match '/images/sort_event_images'              =>        'event_images#sort_images',              via: [:patch],      as: 'sort_event_images'
      # resources :press,                  only: [:index, :new, :edit, :create, :update, :destroy]
      # resources :partners,               only: [:index, :new, :edit, :create, :update, :destroy]
      resources :users,                  only: [:index, :new, :edit, :create, :update, :destroy]
      # resources :categories,             only: [:index, :new, :edit, :create, :update, :destroy]
  end

end

