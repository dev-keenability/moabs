# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180302161413) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "about_amies", force: :cascade do |t|
    t.text "meet_amy"
    t.string "background_img_file_name"
    t.string "background_img_content_type"
    t.integer "background_img_file_size"
    t.datetime "background_img_updated_at"
    t.text "moabs_experience"
    t.string "bottom_ad_one_file_name"
    t.string "bottom_ad_one_content_type"
    t.integer "bottom_ad_one_file_size"
    t.datetime "bottom_ad_one_updated_at"
    t.string "bottom_ad_two_file_name"
    t.string "bottom_ad_two_content_type"
    t.integer "bottom_ad_two_file_size"
    t.datetime "bottom_ad_two_updated_at"
    t.string "bottom_ad_three_file_name"
    t.string "bottom_ad_three_content_type"
    t.integer "bottom_ad_three_file_size"
    t.datetime "bottom_ad_three_updated_at"
    t.string "meta_title"
    t.string "meta_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "bottom_ad_one_link"
    t.string "bottom_ad_two_link"
    t.string "bottom_ad_three_link"
  end

  create_table "about_amy_page_sliders", force: :cascade do |t|
    t.string "image1_link"
    t.string "image1_file_name"
    t.string "image1_content_type"
    t.integer "image1_file_size"
    t.datetime "image1_updated_at"
    t.string "image2_link"
    t.string "image2_file_name"
    t.string "image2_content_type"
    t.integer "image2_file_size"
    t.datetime "image2_updated_at"
    t.string "image3_link"
    t.string "image3_file_name"
    t.string "image3_content_type"
    t.integer "image3_file_size"
    t.datetime "image3_updated_at"
    t.string "image4_link"
    t.string "image4_file_name"
    t.string "image4_content_type"
    t.integer "image4_file_size"
    t.datetime "image4_updated_at"
    t.string "image5_link"
    t.string "image5_file_name"
    t.string "image5_content_type"
    t.integer "image5_file_size"
    t.datetime "image5_updated_at"
    t.string "image6_link"
    t.string "image6_file_name"
    t.string "image6_content_type"
    t.integer "image6_file_size"
    t.datetime "image6_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "about_moabs", force: :cascade do |t|
    t.string "main_image_file_name"
    t.string "main_image_content_type"
    t.integer "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.text "description"
    t.string "meta_title"
    t.string "meta_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "video_link"
  end

  create_table "about_moabs_page_sliders", force: :cascade do |t|
    t.string "image1_link"
    t.string "image1_file_name"
    t.string "image1_content_type"
    t.integer "image1_file_size"
    t.datetime "image1_updated_at"
    t.string "image2_link"
    t.string "image2_file_name"
    t.string "image2_content_type"
    t.integer "image2_file_size"
    t.datetime "image2_updated_at"
    t.string "image3_link"
    t.string "image3_file_name"
    t.string "image3_content_type"
    t.integer "image3_file_size"
    t.datetime "image3_updated_at"
    t.string "image4_link"
    t.string "image4_file_name"
    t.string "image4_content_type"
    t.integer "image4_file_size"
    t.datetime "image4_updated_at"
    t.string "image5_link"
    t.string "image5_file_name"
    t.string "image5_content_type"
    t.integer "image5_file_size"
    t.datetime "image5_updated_at"
    t.string "image6_link"
    t.string "image6_file_name"
    t.string "image6_content_type"
    t.integer "image6_file_size"
    t.datetime "image6_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "blog_indices", force: :cascade do |t|
    t.string "bottom_ad_one_file_name"
    t.string "bottom_ad_one_content_type"
    t.integer "bottom_ad_one_file_size"
    t.datetime "bottom_ad_one_updated_at"
    t.string "bottom_ad_two_file_name"
    t.string "bottom_ad_two_content_type"
    t.integer "bottom_ad_two_file_size"
    t.datetime "bottom_ad_two_updated_at"
    t.string "bottom_ad_three_file_name"
    t.string "bottom_ad_three_content_type"
    t.integer "bottom_ad_three_file_size"
    t.datetime "bottom_ad_three_updated_at"
    t.string "side_ad_file_name"
    t.string "side_ad_content_type"
    t.integer "side_ad_file_size"
    t.datetime "side_ad_updated_at"
    t.string "side_ad_link"
    t.string "bottom_ad_one_link"
    t.string "bottom_ad_two_link"
    t.string "bottom_ad_three_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "blogs", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.text "content_index"
    t.string "title_for_slug"
    t.datetime "pubdate"
    t.string "meta_description"
    t.string "meta_keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "category_id"
    t.string "side_ad_link"
    t.string "side_ad_file_name"
    t.string "side_ad_content_type"
    t.integer "side_ad_file_size"
    t.datetime "side_ad_updated_at"
    t.string "bottom_ad_one_link"
    t.string "bottom_ad_one_file_name"
    t.string "bottom_ad_one_content_type"
    t.integer "bottom_ad_one_file_size"
    t.datetime "bottom_ad_one_updated_at"
    t.string "bottom_ad_two_link"
    t.string "bottom_ad_two_file_name"
    t.string "bottom_ad_two_content_type"
    t.integer "bottom_ad_two_file_size"
    t.datetime "bottom_ad_two_updated_at"
    t.string "bottom_ad_three_link"
    t.string "bottom_ad_three_file_name"
    t.string "bottom_ad_three_content_type"
    t.integer "bottom_ad_three_file_size"
    t.datetime "bottom_ad_three_updated_at"
    t.string "main_image_file_name"
    t.string "main_image_content_type"
    t.integer "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.index ["title_for_slug"], name: "index_blogs_on_title_for_slug"
  end

  create_table "categories", force: :cascade do |t|
    t.string "name"
    t.string "category_slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image_file_name"
    t.string "image_content_type"
    t.integer "image_file_size"
    t.datetime "image_updated_at"
    t.integer "position"
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.integer "assetable_id"
    t.string "assetable_type", limit: 30
    t.string "type", limit: 30
    t.integer "width"
    t.integer "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["assetable_type", "assetable_id"], name: "idx_ckeditor_assetable"
    t.index ["assetable_type", "type", "assetable_id"], name: "idx_ckeditor_assetable_type"
  end

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "phone_number"
    t.text "message"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "zip"
  end

  create_table "event_images", force: :cascade do |t|
    t.string "pic_file_name"
    t.string "pic_content_type"
    t.integer "pic_file_size"
    t.datetime "pic_updated_at"
    t.integer "position"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "event_indices", force: :cascade do |t|
    t.string "bottom_ad_one_file_name"
    t.string "bottom_ad_one_content_type"
    t.integer "bottom_ad_one_file_size"
    t.datetime "bottom_ad_one_updated_at"
    t.string "bottom_ad_two_file_name"
    t.string "bottom_ad_two_content_type"
    t.integer "bottom_ad_two_file_size"
    t.datetime "bottom_ad_two_updated_at"
    t.string "bottom_ad_three_file_name"
    t.string "bottom_ad_three_content_type"
    t.integer "bottom_ad_three_file_size"
    t.datetime "bottom_ad_three_updated_at"
    t.string "side_ad_one_file_name"
    t.string "side_ad_one_content_type"
    t.integer "side_ad_one_file_size"
    t.datetime "side_ad_one_updated_at"
    t.string "side_ad_two_file_name"
    t.string "side_ad_two_content_type"
    t.integer "side_ad_two_file_size"
    t.datetime "side_ad_two_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "bottom_ad_one_link"
    t.string "bottom_ad_two_link"
    t.string "bottom_ad_three_link"
    t.string "side_ad_one_link"
    t.string "side_ad_two_link"
  end

  create_table "event_page_sliders", force: :cascade do |t|
    t.string "image1_link"
    t.string "image1_file_name"
    t.string "image1_content_type"
    t.integer "image1_file_size"
    t.datetime "image1_updated_at"
    t.string "image2_link"
    t.string "image2_file_name"
    t.string "image2_content_type"
    t.integer "image2_file_size"
    t.datetime "image2_updated_at"
    t.string "image3_link"
    t.string "image3_file_name"
    t.string "image3_content_type"
    t.integer "image3_file_size"
    t.datetime "image3_updated_at"
    t.string "image4_link"
    t.string "image4_file_name"
    t.string "image4_content_type"
    t.integer "image4_file_size"
    t.datetime "image4_updated_at"
    t.string "image5_link"
    t.string "image5_file_name"
    t.string "image5_content_type"
    t.integer "image5_file_size"
    t.datetime "image5_updated_at"
    t.string "image6_link"
    t.string "image6_file_name"
    t.string "image6_content_type"
    t.integer "image6_file_size"
    t.datetime "image6_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "event_id"
    t.string "image7_link"
    t.string "image7_file_name"
    t.string "image7_content_type"
    t.integer "image7_file_size"
    t.datetime "image7_updated_at"
    t.string "image8_link"
    t.string "image8_file_name"
    t.string "image8_content_type"
    t.integer "image8_file_size"
    t.datetime "image8_updated_at"
    t.string "image9_link"
    t.string "image9_file_name"
    t.string "image9_content_type"
    t.integer "image9_file_size"
    t.datetime "image9_updated_at"
    t.string "image10_link"
    t.string "image10_file_name"
    t.string "image10_content_type"
    t.integer "image10_file_size"
    t.datetime "image10_updated_at"
    t.string "image11_link"
    t.string "image11_file_name"
    t.string "image11_content_type"
    t.integer "image11_file_size"
    t.datetime "image11_updated_at"
    t.string "image12_link"
    t.string "image12_file_name"
    t.string "image12_content_type"
    t.integer "image12_file_size"
    t.datetime "image12_updated_at"
    t.string "image13_link"
    t.string "image13_file_name"
    t.string "image13_content_type"
    t.integer "image13_file_size"
    t.datetime "image13_updated_at"
    t.string "image14_link"
    t.string "image14_file_name"
    t.string "image14_content_type"
    t.integer "image14_file_size"
    t.datetime "image14_updated_at"
    t.string "image15_link"
    t.string "image15_file_name"
    t.string "image15_content_type"
    t.integer "image15_file_size"
    t.datetime "image15_updated_at"
  end

  create_table "events", force: :cascade do |t|
    t.string "city"
    t.string "main_image_file_name"
    t.string "main_image_content_type"
    t.integer "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.text "event_description"
    t.datetime "event_date"
    t.string "address"
    t.float "latitude"
    t.float "longitude"
    t.string "agenda_title"
    t.string "agenda_description"
    t.string "eventbrite_link"
    t.string "side_ad_one_image_file_name"
    t.string "side_ad_one_image_content_type"
    t.integer "side_ad_one_image_file_size"
    t.datetime "side_ad_one_image_updated_at"
    t.string "side_ad_one_link"
    t.string "ad_one_image_file_name"
    t.string "ad_one_image_content_type"
    t.integer "ad_one_image_file_size"
    t.datetime "ad_one_image_updated_at"
    t.string "ad_one_link"
    t.string "ad_two_image_file_name"
    t.string "ad_two_image_content_type"
    t.integer "ad_two_image_file_size"
    t.datetime "ad_two_image_updated_at"
    t.string "ad_two_link"
    t.string "ad_three_image_file_name"
    t.string "ad_three_image_content_type"
    t.integer "ad_three_image_file_size"
    t.datetime "ad_three_image_updated_at"
    t.string "ad_three_link"
    t.string "title_for_slug"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "location_description"
    t.string "meta_title"
    t.string "meta_description"
    t.string "cover_image_file_name"
    t.string "cover_image_content_type"
    t.integer "cover_image_file_size"
    t.datetime "cover_image_updated_at"
    t.string "icon_file_name"
    t.string "icon_content_type"
    t.integer "icon_file_size"
    t.datetime "icon_updated_at"
    t.string "video_link"
  end

  create_table "events_partners", id: false, force: :cascade do |t|
    t.bigint "partner_id", null: false
    t.bigint "event_id", null: false
    t.index ["event_id", "partner_id"], name: "index_events_partners_on_event_id_and_partner_id"
    t.index ["partner_id", "event_id"], name: "index_events_partners_on_partner_id_and_event_id"
  end

  create_table "faqs", force: :cascade do |t|
    t.string "question"
    t.text "answer"
    t.integer "event_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "home_page_sliders", force: :cascade do |t|
    t.string "image1_link"
    t.string "image1_file_name"
    t.string "image1_content_type"
    t.integer "image1_file_size"
    t.datetime "image1_updated_at"
    t.string "image2_link"
    t.string "image2_file_name"
    t.string "image2_content_type"
    t.integer "image2_file_size"
    t.datetime "image2_updated_at"
    t.string "image3_link"
    t.string "image3_file_name"
    t.string "image3_content_type"
    t.integer "image3_file_size"
    t.datetime "image3_updated_at"
    t.string "image4_link"
    t.string "image4_file_name"
    t.string "image4_content_type"
    t.integer "image4_file_size"
    t.datetime "image4_updated_at"
    t.string "image5_link"
    t.string "image5_file_name"
    t.string "image5_content_type"
    t.integer "image5_file_size"
    t.datetime "image5_updated_at"
    t.string "image6_link"
    t.string "image6_file_name"
    t.string "image6_content_type"
    t.integer "image6_file_size"
    t.datetime "image6_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "home_pages", force: :cascade do |t|
    t.text "description"
    t.string "main_image_file_name"
    t.string "main_image_content_type"
    t.integer "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.string "bottom_ad_one_file_name"
    t.string "bottom_ad_one_content_type"
    t.integer "bottom_ad_one_file_size"
    t.datetime "bottom_ad_one_updated_at"
    t.string "bottom_ad_one_link"
    t.string "bottom_ad_two_file_name"
    t.string "bottom_ad_two_content_type"
    t.integer "bottom_ad_two_file_size"
    t.datetime "bottom_ad_two_updated_at"
    t.string "bottom_ad_two_link"
    t.string "bottom_ad_three_file_name"
    t.string "bottom_ad_three_content_type"
    t.integer "bottom_ad_three_file_size"
    t.datetime "bottom_ad_three_updated_at"
    t.string "bottom_ad_three_link"
    t.string "meta_title"
    t.string "meta_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "video_link"
  end

  create_table "images", force: :cascade do |t|
    t.string "imageable_type"
    t.bigint "imageable_id"
    t.string "name"
    t.string "pic_file_name"
    t.string "pic_content_type"
    t.integer "pic_file_size"
    t.datetime "pic_updated_at"
    t.boolean "primary"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "position"
    t.string "slider"
    t.index ["imageable_type", "imageable_id"], name: "index_images_on_imageable_type_and_imageable_id"
  end

  create_table "partner_page_sliders", force: :cascade do |t|
    t.string "image1_link"
    t.string "image1_file_name"
    t.string "image1_content_type"
    t.integer "image1_file_size"
    t.datetime "image1_updated_at"
    t.string "image2_link"
    t.string "image2_file_name"
    t.string "image2_content_type"
    t.integer "image2_file_size"
    t.datetime "image2_updated_at"
    t.string "image3_link"
    t.string "image3_file_name"
    t.string "image3_content_type"
    t.integer "image3_file_size"
    t.datetime "image3_updated_at"
    t.string "image4_link"
    t.string "image4_file_name"
    t.string "image4_content_type"
    t.integer "image4_file_size"
    t.datetime "image4_updated_at"
    t.string "image5_link"
    t.string "image5_file_name"
    t.string "image5_content_type"
    t.integer "image5_file_size"
    t.datetime "image5_updated_at"
    t.string "image6_link"
    t.string "image6_file_name"
    t.string "image6_content_type"
    t.integer "image6_file_size"
    t.datetime "image6_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "partner_with_us", force: :cascade do |t|
    t.string "main_image_file_name"
    t.string "main_image_content_type"
    t.integer "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.text "description"
    t.string "testimonial"
    t.string "testimonial_author"
    t.string "meta_title"
    t.string "meta_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "partners", force: :cascade do |t|
    t.string "name"
    t.string "weblink"
    t.text "description"
    t.string "testimonial"
    t.string "testimonial_author"
    t.string "title_for_slug"
    t.string "main_image_file_name"
    t.string "main_image_content_type"
    t.integer "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "meta_description"
  end

  create_table "press", force: :cascade do |t|
    t.string "title"
    t.text "content"
    t.text "content_index"
    t.string "title_for_slug"
    t.datetime "pubdate"
    t.string "meta_description"
    t.string "meta_keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "side_ad_link"
    t.string "side_ad_file_name"
    t.string "side_ad_content_type"
    t.integer "side_ad_file_size"
    t.datetime "side_ad_updated_at"
    t.string "bottom_ad_one_link"
    t.string "bottom_ad_one_file_name"
    t.string "bottom_ad_one_content_type"
    t.integer "bottom_ad_one_file_size"
    t.datetime "bottom_ad_one_updated_at"
    t.string "bottom_ad_two_link"
    t.string "bottom_ad_two_file_name"
    t.string "bottom_ad_two_content_type"
    t.integer "bottom_ad_two_file_size"
    t.datetime "bottom_ad_two_updated_at"
    t.string "bottom_ad_three_link"
    t.string "bottom_ad_three_file_name"
    t.string "bottom_ad_three_content_type"
    t.integer "bottom_ad_three_file_size"
    t.datetime "bottom_ad_three_updated_at"
    t.string "main_image_file_name"
    t.string "main_image_content_type"
    t.integer "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.index ["title_for_slug"], name: "index_press_on_title_for_slug"
  end

  create_table "press_indices", force: :cascade do |t|
    t.string "bottom_ad_one_file_name"
    t.string "bottom_ad_one_content_type"
    t.integer "bottom_ad_one_file_size"
    t.datetime "bottom_ad_one_updated_at"
    t.string "bottom_ad_two_file_name"
    t.string "bottom_ad_two_content_type"
    t.integer "bottom_ad_two_file_size"
    t.datetime "bottom_ad_two_updated_at"
    t.string "bottom_ad_three_file_name"
    t.string "bottom_ad_three_content_type"
    t.integer "bottom_ad_three_file_size"
    t.datetime "bottom_ad_three_updated_at"
    t.string "side_ad_file_name"
    t.string "side_ad_content_type"
    t.integer "side_ad_file_size"
    t.datetime "side_ad_updated_at"
    t.string "side_ad_link"
    t.string "bottom_ad_one_link"
    t.string "bottom_ad_two_link"
    t.string "bottom_ad_three_link"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "press_pages", force: :cascade do |t|
    t.string "main_image_file_name"
    t.string "main_image_content_type"
    t.integer "main_image_file_size"
    t.datetime "main_image_updated_at"
    t.string "video_link"
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "admin"
    t.string "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
