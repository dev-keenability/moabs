class AddImageToPress < ActiveRecord::Migration[5.1]
  def change
    add_column :press, :side_ad_link, :string
    add_attachment :press, :side_ad
    add_column :press, :bottom_ad_one_link, :string
    add_attachment :press, :bottom_ad_one
    add_column :press, :bottom_ad_two_link, :string
    add_attachment :press, :bottom_ad_two
    add_column :press, :bottom_ad_three_link, :string
    add_attachment :press, :bottom_ad_three
  end
end
