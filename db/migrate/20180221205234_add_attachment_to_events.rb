class AddAttachmentToEvents < ActiveRecord::Migration[5.1]
  def change
    add_attachment :events, :cover_image
  end
end
