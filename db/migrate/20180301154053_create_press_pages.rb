class CreatePressPages < ActiveRecord::Migration[5.1]
  def change
    create_table :press_pages do |t|
      t.attachment :main_image
      t.string :video_link
      t.text :description

      t.timestamps
    end
  end
end
