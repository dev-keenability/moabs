class CreateEventIndices < ActiveRecord::Migration[5.1]
  def change
    create_table :event_indices do |t|
      t.attachment :bottom_ad_one
      t.attachment :bottom_ad_two
      t.attachment :bottom_ad_three
      t.attachment :side_ad_one
      t.attachment :side_ad_two

      t.timestamps
    end
  end
end
