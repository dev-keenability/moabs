class CreateEventImages < ActiveRecord::Migration[5.1]
  def change
    create_table :event_images do |t|
      t.attachment :pic
      t.integer :position

      t.timestamps
    end
  end
end
