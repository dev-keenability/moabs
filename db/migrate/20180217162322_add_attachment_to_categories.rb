class AddAttachmentToCategories < ActiveRecord::Migration[5.1]
  def up
    add_attachment :categories, :image
  end
end
