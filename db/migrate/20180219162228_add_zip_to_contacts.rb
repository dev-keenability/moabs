class AddZipToContacts < ActiveRecord::Migration[5.1]
  def up
    add_column :contacts, :zip, :string
  end
end
