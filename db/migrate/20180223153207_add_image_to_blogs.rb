class AddImageToBlogs < ActiveRecord::Migration[5.1]
  def change
    add_column :blogs, :side_ad_link, :string
    add_attachment :blogs, :side_ad
    add_column :blogs, :bottom_ad_one_link, :string
    add_attachment :blogs, :bottom_ad_one
    add_column :blogs, :bottom_ad_two_link, :string
    add_attachment :blogs, :bottom_ad_two
    add_column :blogs, :bottom_ad_three_link, :string
    add_attachment :blogs, :bottom_ad_three

  end
end
