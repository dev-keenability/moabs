class CreateBlogs < ActiveRecord::Migration[5.1]
  def change
    create_table :blogs do |t|
      t.string :title
      t.text :content
      t.text :content_index
      t.string :title_for_slug
      t.string :main_image
      t.datetime :pubdate
      t.string :meta_description
      t.string :meta_keywords
      t.string :title_for_slug

      t.timestamps
    end
    add_index :blogs, :title_for_slug
  end
end
