class CreateEvents < ActiveRecord::Migration[5.1]
  def change
    create_table :events do |t|
      t.string :city
      t.attachment :main_image
      t.text :event_description
      t.datetime :event_date
      t.string :address
      t.float :latitude
      t.float :longitude
      t.string :agenda_title
      t.string :agenda_description
      t.string :eventbrite_link
      t.attachment :side_ad_one_image
      t.string :side_ad_one_link
      t.attachment :ad_one_image
      t.string :ad_one_link
      t.attachment :ad_two_image
      t.string :ad_two_link
      t.attachment :ad_three_image
      t.string :ad_three_link
      t.string :title_for_slug

      t.timestamps
    end
  end
end
