class CreatePartners < ActiveRecord::Migration[5.1]
  def change
    create_table :partners do |t|
      t.string :name
      t.string :weblink
      t.text :description
      t.string :testimonial
      t.string :testimonial_author
      t.string :title_for_slug
      t.attachment :main_image

      t.timestamps
    end
  end
end
