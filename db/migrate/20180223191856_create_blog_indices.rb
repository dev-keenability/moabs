class CreateBlogIndices < ActiveRecord::Migration[5.1]
  def change
    create_table :blog_indices do |t|
      t.attachment :bottom_ad_one
      t.attachment :bottom_ad_two
      t.attachment :bottom_ad_three
      t.attachment :side_ad
      t.string :side_ad_link
      t.string :bottom_ad_one_link
      t.string :bottom_ad_two_link
      t.string :bottom_ad_three_link

      t.timestamps
    end
    create_table :press_indices do |t|
      t.attachment :bottom_ad_one
      t.attachment :bottom_ad_two
      t.attachment :bottom_ad_three
      t.attachment :side_ad
      t.string :side_ad_link
      t.string :bottom_ad_one_link
      t.string :bottom_ad_two_link
      t.string :bottom_ad_three_link

      t.timestamps
    end
  end
end
