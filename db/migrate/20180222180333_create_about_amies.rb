class CreateAboutAmies < ActiveRecord::Migration[5.1]
  def change
    create_table :about_amies do |t|
      t.text :meet_amy
      t.attachment :background_img
      t.text :moabs_experience
      t.attachment :bottom_ad_one
      t.attachment :bottom_ad_two
      t.attachment :bottom_ad_three
      t.string :meta_title
      t.string :meta_description

      t.timestamps
    end
  end
end
