class AddSliderToImages < ActiveRecord::Migration[5.1]
  def change
    add_column :images, :slider, :string
  end
end
