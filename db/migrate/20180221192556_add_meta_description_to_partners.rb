class AddMetaDescriptionToPartners < ActiveRecord::Migration[5.1]
  def change
    add_column :partners, :meta_description, :string
  end
end
