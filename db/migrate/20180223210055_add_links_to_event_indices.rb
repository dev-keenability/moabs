class AddLinksToEventIndices < ActiveRecord::Migration[5.1]
  def change
    add_column :event_indices, :bottom_ad_one_link, :string
    add_column :event_indices, :bottom_ad_two_link, :string
    add_column :event_indices, :bottom_ad_three_link, :string
    add_column :event_indices, :side_ad_one_link, :string
    add_column :event_indices, :side_ad_two_link, :string
  end
end
