class CreateJoinTableEventsPartners < ActiveRecord::Migration[5.1]
  def change
    create_join_table :partners, :events do |t|
      t.index [:partner_id, :event_id]
      t.index [:event_id, :partner_id]
    end
  end
end
