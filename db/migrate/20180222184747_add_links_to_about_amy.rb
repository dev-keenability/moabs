class AddLinksToAboutAmy < ActiveRecord::Migration[5.1]
  def change
    add_column :about_amies, :bottom_ad_one_link, :string
    add_column :about_amies, :bottom_ad_two_link, :string
    add_column :about_amies, :bottom_ad_three_link, :string
  end
end
