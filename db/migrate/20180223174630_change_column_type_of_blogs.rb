class ChangeColumnTypeOfBlogs < ActiveRecord::Migration[5.1]
  def change
    remove_column :blogs, :main_image
    add_attachment :blogs, :main_image
    remove_column :press, :main_image
    add_attachment :press, :main_image
  end
end
