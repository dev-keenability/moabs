class AddStuffToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :location_description, :text
    add_column :events, :meta_title, :string
    add_column :events, :meta_description, :string
  end
end
