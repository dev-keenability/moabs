class AddVideoLinksToModelz < ActiveRecord::Migration[5.1]
  def change
    add_column :events,       :video_link, :string
    add_column :home_pages,   :video_link, :string
    add_column :about_moabs,  :video_link, :string
  end
end
