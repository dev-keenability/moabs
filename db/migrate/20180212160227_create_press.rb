class CreatePress < ActiveRecord::Migration[5.1]
  def change
    create_table :press do |t|
      t.string :title
      t.text :content
      t.text :content_index
      t.string :title_for_slug
      t.string :main_image
      t.datetime :pubdate
      t.string :meta_description
      t.string :meta_keywords

      t.timestamps
    end
    add_index :press, :title_for_slug
  end
end
