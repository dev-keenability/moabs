class AddIconToEvents < ActiveRecord::Migration[5.1]
  def change
    add_attachment :events, :icon
  end
end
