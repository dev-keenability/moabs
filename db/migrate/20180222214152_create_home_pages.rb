class CreateHomePages < ActiveRecord::Migration[5.1]
  def change
    create_table :home_pages do |t|
      t.text :description
      t.attachment :main_image
      t.attachment :bottom_ad_one
      t.string :bottom_ad_one_link
      t.attachment :bottom_ad_two
      t.string :bottom_ad_two_link
      t.attachment :bottom_ad_three
      t.string :bottom_ad_three_link
      t.string :meta_title
      t.string :meta_description

      t.timestamps
    end
  end
end
