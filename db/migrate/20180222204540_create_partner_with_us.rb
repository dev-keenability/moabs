class CreatePartnerWithUs < ActiveRecord::Migration[5.1]
  def change
    create_table :partner_with_us do |t|
      t.attachment :main_image
      t.text :description
      t.string :testimonial
      t.string :testimonial_author
      t.string :meta_title
      t.string :meta_description

      t.timestamps
    end
  end
end
