class CreateAboutMoabs < ActiveRecord::Migration[5.1]
  def change
    create_table :about_moabs do |t|
      t.attachment :main_image
      t.text :description
      t.string :meta_title
      t.string :meta_description

      t.timestamps
    end
  end
end
