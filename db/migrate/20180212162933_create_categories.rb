class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :name
      t.string :category_slug
      t.string :main_image

      t.timestamps
    end
  end
end
