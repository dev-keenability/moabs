class Event < ApplicationRecord
  has_many :faqs, dependent: :destroy
  has_and_belongs_to_many :partners
  before_save :title_url_it
  geocoded_by :address
  after_validation :geocode, :if => :address_changed?

  # has_many :images, dependent: :destroy
  # accepts_nested_attributes_for :images, allow_destroy: true

  has_many :images, as: :imageable, :class_name => "Event::Asset", dependent: :destroy

    Paperclip.interpolates :id do |attachment, style|
      attachment.instance.id
    end

    has_attached_file :cover_image, 
    :preserve_files => false,
    :path => "#{Rails.env}/Event/:id/cover_image/:style/:filename",
    :styles => { 
      :medium => ["295x342#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :cover_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true



    has_attached_file :icon, 
    :preserve_files => false,
    :path => "#{Rails.env}/Event/:id/icon/:style/:filename",
    :styles => { 
      :medium => ["150x150#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :icon, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true


    
    has_attached_file :main_image, 
    :preserve_files => false,
    :path => "#{Rails.env}/Event/:id/main_image/:style/:filename",
    :styles => { 
      :medium => ["262x175#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true




    has_attached_file :side_ad_one_image, 
    :preserve_files => false,
    :path => "#{Rails.env}/Event/:id/side_ad_one_image/:style/:filename",
    :styles => { 
      :medium => ["160x600#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }



    has_attached_file :ad_one_image, 
    :preserve_files => false,
    :path => "#{Rails.env}/Event/:id/ad_one_image/:style/:filename",
    :styles => { 
      :medium => ["300x250#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }



    has_attached_file :ad_two_image, 
    :preserve_files => false,
    :path => "#{Rails.env}/Event/:id/ad_two_image/:style/:filename",
    :styles => { 
      :medium => ["300x250#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }



    has_attached_file :ad_three_image, 
    :preserve_files => false,
    :path => "#{Rails.env}/Event/:id/ad_three_image/:style/:filename",
    :styles => { 
      :medium => ["300x250#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }



  def title_url_it
    # mytitle = event_date.strftime("%Y-%m-%d")
    # mytitle += "-" + city.downcase.squish.parameterize
    # self.title_for_slug = mytitle

    self.title_for_slug = city.downcase.squish.parameterize
  end

  
end
