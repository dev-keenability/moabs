class Partner < ApplicationRecord
  has_and_belongs_to_many :events
  before_save :title_url_it

  has_many :images, as: :imageable, :class_name => "Partner::Asset", dependent: :destroy

  Paperclip.interpolates :id do |attachment, style|
      attachment.instance.id
    end

    has_attached_file :main_image, 
    :preserve_files => false,
    :path => "#{Rails.env}/Partner/:id/main_image/:style/:filename",
    :styles => { 
      :small => ["262x205#", :jpg], 
      :medium => ["300x300#", :jpg], 
      :large => ["1200x800#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true

  def title_url_it
    self.title_for_slug = name.downcase.squish.parameterize
  end

end
