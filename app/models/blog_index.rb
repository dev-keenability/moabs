class BlogIndex < ApplicationRecord

    has_attached_file :bottom_ad_one, 
    :preserve_files => false,
    :path => "#{Rails.env}/BlogIndex/bottom_ad_one/:style/:filename",
    :styles => { 
      :medium => ["300x250#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :bottom_ad_one, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }




    has_attached_file :bottom_ad_two, 
    :preserve_files => false,
    :path => "#{Rails.env}/BlogIndex/bottom_ad_two/:style/:filename",
    :styles => { 
      :medium => ["300x250#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :bottom_ad_two, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }




    has_attached_file :bottom_ad_three, 
    :preserve_files => false,
    :path => "#{Rails.env}/BlogIndex/bottom_ad_three/:style/:filename",
    :styles => { 
      :medium => ["300x250#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :bottom_ad_three, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }





    has_attached_file :side_ad, 
    :preserve_files => false,
    :path => "#{Rails.env}/BlogIndex/side_ad/:style/:filename",
    :styles => { 
      :medium => ["160x600#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :side_ad, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }


end
