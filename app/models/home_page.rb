class HomePage < ApplicationRecord
  validates_presence_of :description

  has_many :images, as: :imageable, :class_name => "HomePage::Asset", dependent: :destroy

  has_attached_file :main_image, 
  :preserve_files => false,
  :path => "#{Rails.env}/HomePage/main_image/:style/:filename",
  :styles => { 
    :medium => ["800x400#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true




  has_attached_file :bottom_ad_one, 
  :preserve_files => false,
  :path => "#{Rails.env}/HomePage/bottom_ad_one/:style/:filename",
  :styles => { 
    :medium => ["300x250#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :bottom_ad_one, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }





  has_attached_file :bottom_ad_two, 
  :preserve_files => false,
  :path => "#{Rails.env}/HomePage/bottom_ad_two/:style/:filename",
  :styles => { 
    :medium => ["300x250#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :bottom_ad_two, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }




  has_attached_file :bottom_ad_three, 
  :preserve_files => false,
  :path => "#{Rails.env}/HomePage/bottom_ad_three/:style/:filename",
  :styles => { 
    :medium => ["300x250#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :bottom_ad_three, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }




end
