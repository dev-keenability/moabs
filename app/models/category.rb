class Category < ApplicationRecord
  has_many :blogs, :dependent => :nullify
  validates :name, uniqueness: {case_sensitive: false}

  before_save :title_url_it

  # has_attached_file :image,
  #                         styles: { medium: ["300x190#", :jpg] }
  # validates_attachment :image, content_type: { content_type: ["image/jpg", "image/jpeg", "image/png", "image/gif"] }


    Paperclip.interpolates :id do |attachment, style|
      attachment.instance.id
    end

    has_attached_file :image, 
    :preserve_files => false,
    :path => "#{Rails.env}/Category/:id/image/:style/:filename",
    :styles => { 
      :medium => ["360x240#", :jpg]
    },
    :convert_options => {
     # :medium => "-quality 80 -interlace Plane",
     # :small => "-quality 80 -interlace Plane",
     # :thumb => "-quality 80 -interlace Plane",
     # :facebook_meta_tag => "-quality 80 -interlace Plane" 
    },
    :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

    validates_attachment :image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true




  def title_url_it
    self.category_slug = name.downcase.squish.parameterize
  end

end
