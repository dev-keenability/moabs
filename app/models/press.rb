class Press < ApplicationRecord

  validates :title, uniqueness: {case_sensitive: false}

  before_save :title_url_it
  before_save :remove_img

  has_many :images, as: :imageable, :class_name => "Press::Asset", dependent: :destroy
  accepts_nested_attributes_for :images, allow_destroy: true
  


  has_attached_file :main_image, 
  :preserve_files => false,
  :path => "#{Rails.env}/Press/:id/main_image/:style/:filename",
  :styles => { 
    :medium => ["360x270#", :jpg],
    :large => ["1200x800#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true



  has_attached_file :bottom_ad_one, 
  :preserve_files => false,
  :path => "#{Rails.env}/Press/:id/bottom_ad_one/:style/:filename",
  :styles => { 
    :medium => ["300x250#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :bottom_ad_one, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true




  has_attached_file :bottom_ad_two, 
  :preserve_files => false,
  :path => "#{Rails.env}/Press/:id/bottom_ad_two/:style/:filename",
  :styles => { 
    :medium => ["300x250#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :bottom_ad_two, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true




  has_attached_file :bottom_ad_three, 
  :preserve_files => false,
  :path => "#{Rails.env}/Press/:id/bottom_ad_three/:style/:filename",
  :styles => { 
    :medium => ["300x250#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :bottom_ad_three, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true





  has_attached_file :side_ad, 
  :preserve_files => false,
  :path => "#{Rails.env}/Press/:id/side_ad/:style/:filename",
  :styles => { 
    :medium => ["160x600#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :side_ad, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true









  #This is for SEO purposes, you want the url to be the title of the blog instead of its ID
  def title_url_it
    self.title_for_slug = title.downcase.squish.parameterize
  end

  def remove_img
    nokogiri_html_document =  Nokogiri::HTML(self.content) #heroku does not like tmp directory
    nokogiri_html_document.search('img').each do |img|
      img.remove
    end
    self.content_index = nokogiri_html_document
    #note that if you call self.save here, you might run into infinite loops (stack level too deep), even for after_save, after_create and after_update callbacks
  end

end