class PressPage < ApplicationRecord
  has_many :images, as: :imageable, :class_name => "PressPage::Asset", dependent: :destroy

  has_attached_file :main_image, 
  :preserve_files => false,
  :path => "#{Rails.env}/PressPage/main_image/:style/:filename",
  :styles => { 
    :medium => ["300x200#", :jpg],
    :huge => ["1600x663#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }

end
