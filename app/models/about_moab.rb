class AboutMoab < ApplicationRecord

  validates_presence_of :description

  has_many :images, as: :imageable, :class_name => "AboutMoab::Asset", dependent: :destroy

  has_attached_file :main_image, 
  :preserve_files => false,
  :path => "#{Rails.env}/AboutMoab/main_image/:style/:filename",
  :styles => { 
    :medium => ["360x240#", :jpg]
  },
  :convert_options => {
   # :medium => "-quality 80 -interlace Plane",
   # :small => "-quality 80 -interlace Plane",
   # :thumb => "-quality 80 -interlace Plane",
   # :facebook_meta_tag => "-quality 80 -interlace Plane" 
  },
  :s3_headers => { 'Cache-Control' => 'max-age=315576000', 'Expires' => 10.years.from_now.httpdate }

  validates_attachment :main_image, content_type: { content_type: ["image/jpeg", "image/gif", "image/png", "image/jpg"] }, size: { in: 0..3000.kilobytes }, presence: true
  
end
