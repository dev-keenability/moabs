// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery-fileupload/basic
//= require jquery-fileupload/vendor/tmpl
//= require rails-ujs
//= require plugins/jquery.readySelector
//= require ckeditor/init
//= require bootstrap
//= require light-gallery
//= require owl.carousel.min
//= require instafeed.min
//= require turbolinks
//= require_tree .


//Subscribe modal after 15 seconds expires in 7 days, ("https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js")
$(window).load(function(){
    if (Cookies.get('modal_shown') == null) {
        setTimeout(function () {
            $('#myModal').modal('show');
        }, 15000);
        Cookies.set('modal_shown', 'yes', { expires: 7 });
    };
    if (Cookies.get('modal_shown_two') == null) {
        setTimeout(function () {
            $('#myModal').modal('show');
        }, 120000);
        Cookies.set('modal_shown_two', 'yes', { expires: 7 });
    };
});



$(document).on('turbolinks:load', function() {
  var today = new Date();
  console.log($('body').attr('class') + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds());

  if ($('body.home.index').length > 0) {
    console.log('index');
  }

  if ($('body.home.about').length > 0) {
    console.log('about');
  }

  $('.homeeventimghover').on({
      mouseenter: function() {
          $(this).find('img').show();
          $(this).find('.homeeventimgdiv').hide();
      },
      mouseleave: function() {
          $(this).find('.homeeventimgdiv').show();
          $(this).find('img').hide();
      }
  });


  $('.carousel').carousel({
    interval: 4000
  });








    $('#event_images,#partner_images,#home_page_images,#partner_with_u_images,#about_amy_images,#about_moab_images,#press_images').sortable({
      // axis: "y" // this option is if you only want to restrict sorting vertically, maybe you are sorting a list
      cursor: "move",
      // handle: '.handle',  // this is if you want to change the handle
      update: function( event, ui ) {
        $.ajax({
          type: "PATCH",
          url: $(this).data('update-url'), // path to file
          data: $(this).sortable('serialize'),
        })
      }
    });


    $('#event_dropzone').fileupload({
      dataType: 'script',
      dropZone: $('#event_dropzone'),
      add: function (e, data) {
        types = /(\.|\/)(gif|jpg|jpeg|png|mov|mpeg|mpeg4|avi)$/i;
        file = data.files[0];
        if (types.test(file.type) || types.test(file.name)) {
          data.context = $(tmpl("template-upload", file));
          $("#new_event_asset").append(data.context);
          data.submit();
        }
        else { 
          alert(file.name + " is not a image or movie file.");
        }
      },
      progress: function (e, data) {
        if (data.context) {
          // FYI: data.files[0].name is how you retrieve the name of the file being uploaded
          // The second param in parseInt is the radix which is another name for base, i.e. 2 for binary, 10 for decimal, 16 for hexadecimal, 
          // progress = parseInt(data.loaded / data.total * 100, 10);
          // data.context.find('.progress-bar').css('width', progress + '%').text(progress + '%');
          var percent = Math.round((data.loaded / data.total) * 100);
          data.context.find('.progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%')
        }
      },
      done: function (e, data) {
        // data.context.text('Uploaded ' + data.files[0].name);
        data.context.text(''); // this is so that it replaces the progress bar with an empty string, aka removes the progress bar once done.
      }
    });

    $('#partner_dropzone').fileupload({
      dataType: 'script',
      dropZone: $('#partner_dropzone'),
      add: function (e, data) {
        types = /(\.|\/)(gif|jpg|jpeg|png|mov|mpeg|mpeg4|avi)$/i;
        file = data.files[0];
        if (types.test(file.type) || types.test(file.name)) {
          data.context = $(tmpl("template-upload", file));
          $("#new_partner_asset").append(data.context);
          data.submit();
        }
        else { 
          alert(file.name + " is not a image or movie file.");
        }
      },
      progress: function (e, data) {
        if (data.context) {
          // FYI: data.files[0].name is how you retrieve the name of the file being uploaded
          // The second param in parseInt is the radix which is another name for base, i.e. 2 for binary, 10 for decimal, 16 for hexadecimal, 
          // progress = parseInt(data.loaded / data.total * 100, 10);
          // data.context.find('.progress-bar').css('width', progress + '%').text(progress + '%');
          var percent = Math.round((data.loaded / data.total) * 100);
          data.context.find('.progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%')
        }
      },
      done: function (e, data) {
        // data.context.text('Uploaded ' + data.files[0].name);
        data.context.text(''); // this is so that it replaces the progress bar with an empty string, aka removes the progress bar once done.
      }
    });

    $('#home_page_dropzone').fileupload({
      dataType: 'script',
      dropZone: $('#home_page_dropzone'),
      add: function (e, data) {
        types = /(\.|\/)(gif|jpg|jpeg|png|mov|mpeg|mpeg4|avi)$/i;
        file = data.files[0];
        if (types.test(file.type) || types.test(file.name)) {
          data.context = $(tmpl("template-upload", file));
          $("#new_home_page_asset").append(data.context);
          data.submit();
        }
        else { 
          alert(file.name + " is not a image or movie file.");
        }
      },
      progress: function (e, data) {
        if (data.context) {
          // FYI: data.files[0].name is how you retrieve the name of the file being uploaded
          // The second param in parseInt is the radix which is another name for base, i.e. 2 for binary, 10 for decimal, 16 for hexadecimal, 
          // progress = parseInt(data.loaded / data.total * 100, 10);
          // data.context.find('.progress-bar').css('width', progress + '%').text(progress + '%');
          var percent = Math.round((data.loaded / data.total) * 100);
          data.context.find('.progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%')
        }
      },
      done: function (e, data) {
        // data.context.text('Uploaded ' + data.files[0].name);
        data.context.text(''); // this is so that it replaces the progress bar with an empty string, aka removes the progress bar once done.
      }
    });

    $('#about_amy_dropzone').fileupload({
      dataType: 'script',
      dropZone: $('#about_amy_dropzone'),
      add: function (e, data) {
        types = /(\.|\/)(gif|jpg|jpeg|png|mov|mpeg|mpeg4|avi)$/i;
        file = data.files[0];
        if (types.test(file.type) || types.test(file.name)) {
          data.context = $(tmpl("template-upload", file));
          $("#new_about_amy_asset").append(data.context);
          data.submit();
        }
        else { 
          alert(file.name + " is not a image or movie file.");
        }
      },
      progress: function (e, data) {
        if (data.context) {
          // FYI: data.files[0].name is how you retrieve the name of the file being uploaded
          // The second param in parseInt is the radix which is another name for base, i.e. 2 for binary, 10 for decimal, 16 for hexadecimal, 
          // progress = parseInt(data.loaded / data.total * 100, 10);
          // data.context.find('.progress-bar').css('width', progress + '%').text(progress + '%');
          var percent = Math.round((data.loaded / data.total) * 100);
          data.context.find('.progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%')
        }
      },
      done: function (e, data) {
        // data.context.text('Uploaded ' + data.files[0].name);
        data.context.text(''); // this is so that it replaces the progress bar with an empty string, aka removes the progress bar once done.
      }
    });

    $('#press_image_dropzone').fileupload({
      dataType: 'script',
      dropZone: $('#press_image_dropzone'),
      add: function (e, data) {
        types = /(\.|\/)(gif|jpg|jpeg|png|mov|mpeg|mpeg4|avi)$/i;
        file = data.files[0];
        if (types.test(file.type) || types.test(file.name)) {
          data.context = $(tmpl("template-upload", file));
          $("#new_press_page_asset").append(data.context);
          data.submit();
        }
        else { 
          alert(file.name + " is not a image or movie file.");
        }
      },
      progress: function (e, data) {
        if (data.context) {
          // FYI: data.files[0].name is how you retrieve the name of the file being uploaded
          // The second param in parseInt is the radix which is another name for base, i.e. 2 for binary, 10 for decimal, 16 for hexadecimal, 
          // progress = parseInt(data.loaded / data.total * 100, 10);
          // data.context.find('.progress-bar').css('width', progress + '%').text(progress + '%');
          var percent = Math.round((data.loaded / data.total) * 100);
          data.context.find('.progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%')
        }
      },
      done: function (e, data) {
        // data.context.text('Uploaded ' + data.files[0].name);
        data.context.text(''); // this is so that it replaces the progress bar with an empty string, aka removes the progress bar once done.
      }
    });

    $('#about_moab_dropzone').fileupload({
      dataType: 'script',
      dropZone: $('#about_moab_dropzone'),
      add: function (e, data) {
        types = /(\.|\/)(gif|jpg|jpeg|png|mov|mpeg|mpeg4|avi)$/i;
        file = data.files[0];
        if (types.test(file.type) || types.test(file.name)) {
          data.context = $(tmpl("template-upload", file));
          $("#new_about_moab_asset").append(data.context);
          data.submit();
        }
        else { 
          alert(file.name + " is not a image or movie file.");
        }
      },
      progress: function (e, data) {
        if (data.context) {
          // FYI: data.files[0].name is how you retrieve the name of the file being uploaded
          // The second param in parseInt is the radix which is another name for base, i.e. 2 for binary, 10 for decimal, 16 for hexadecimal, 
          // progress = parseInt(data.loaded / data.total * 100, 10);
          // data.context.find('.progress-bar').css('width', progress + '%').text(progress + '%');
          var percent = Math.round((data.loaded / data.total) * 100);
          data.context.find('.progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%')
        }
      },
      done: function (e, data) {
        // data.context.text('Uploaded ' + data.files[0].name);
        data.context.text(''); // this is so that it replaces the progress bar with an empty string, aka removes the progress bar once done.
      }
    });

    $('#partner_with_u_dropzone').fileupload({
      dataType: 'script',
      dropZone: $('#partner_with_u_dropzone'),
      add: function (e, data) {
        types = /(\.|\/)(gif|jpg|jpeg|png|mov|mpeg|mpeg4|avi)$/i;
        file = data.files[0];
        if (types.test(file.type) || types.test(file.name)) {
          data.context = $(tmpl("template-upload", file));
          $("#new_partner_with_u_asset").append(data.context);
          data.submit();
        }
        else { 
          alert(file.name + " is not a image or movie file.");
        }
      },
      progress: function (e, data) {
        if (data.context) {
          // FYI: data.files[0].name is how you retrieve the name of the file being uploaded
          // The second param in parseInt is the radix which is another name for base, i.e. 2 for binary, 10 for decimal, 16 for hexadecimal, 
          // progress = parseInt(data.loaded / data.total * 100, 10);
          // data.context.find('.progress-bar').css('width', progress + '%').text(progress + '%');
          var percent = Math.round((data.loaded / data.total) * 100);
          data.context.find('.progress-bar').attr('aria-valuenow', percent).css('width', percent + '%').text(percent + '%')
        }
      },
      done: function (e, data) {
        // data.context.text('Uploaded ' + data.files[0].name);
        data.context.text(''); // this is so that it replaces the progress bar with an empty string, aka removes the progress bar once done.
      }
    });




  


  if ($('body.blogs.index,body.blogs.index_category,body.press.index,body.events.index').length > 0) {
    var lp = $('.style_paginate').data('last-page');
    if ($('.current').text().indexOf('Page') < 0) {
      $('.current').prepend('Page ').append(' of ').append(lp);
    }
    
    $('.previous_page').html('<span class="blog_page_arrow"><</span>');
    $('.next_page').html('<span class="blog_page_arrow">></span>');

    $last_page = $(".pagination a:nth-last-child(2)");
    var last_page_href = $last_page.attr('href');
    if ((last_page_href) && $('.last_page').text().indexOf('last') < 0) {
      $('.pagination').append('<a  class="last_page" href="' + last_page_href + '">last</a>');
    }
    
    $first_page = $(".pagination a:nth-child(2)");
    if ($first_page.text() == 1) {
      var first_page_href = $first_page.attr('href');
      $('.pagination').prepend('<a class="first_page" href="' + first_page_href + '">first</a>');
    }
  }



  if ($('body.events.index').length > 0 || $('body.events.our_events').length > 0) {

      try {google;} catch (e){location.reload();}

      var initializeMap = (function() {

          // https://snazzymaps.com
            window.mystyle = [
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#444444"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "hue": "#ff0000"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "geometry",
                "stylers": [
                    {
                        "lightness": "100"
                    }
                ]
            },
            {
                "featureType": "landscape.man_made",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "lightness": "100"
                    }
                ]
            },
            {
                "featureType": "landscape.natural",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.landcover",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry",
                "stylers": [
                    {
                        "lightness": "100"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "off"
                    },
                    {
                        "lightness": "23"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": -100
                    },
                    {
                        "lightness": 45
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "simplified"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffd900"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "labels.icon",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#ffd900"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "visibility": "on"
                    },
                    {
                        "color": "#cccccc"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            }
        ]



        var uluru = {lat: 44.7313707, lng: -92.5406341};
        var map = new google.maps.Map(document.getElementById('eventmap'), {
          zoom: 4,
          center: uluru,
          styles: mystyle
        });

        // To pan aka offset center of the map
        map.panBy(-50,50);

        var icons = {
          restaurant: {
            icon: 'https://s3.us-east-2.amazonaws.com/moabs1/general/MOABS_favicon.jpg'
          },
          shopping: {
            icon: 'https://s3.us-east-2.amazonaws.com/moabs1/general/MOABS_favicon.jpg'
          },
          lodging: {
            icon: 'https://s3.us-east-2.amazonaws.com/moabs1/general/MOABS_favicon.jpg'
          },
          bar: {
            icon: 'https://s3.us-east-2.amazonaws.com/moabs1/general/MOABS_favicon.jpg'
          },
          coffee: {
            icon: 'https://s3.us-east-2.amazonaws.com/moabs1/general/MOABS_favicon.jpg'
          }
        };

        var infowindow;   

        function addMarker(feature) {

          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(feature.latitude,feature.longitude),
            icon: 'https://s3.us-east-2.amazonaws.com/moabs1/general/MOABS_favicon.jpg',
            optimized: false,
            zIndex:Date.now(), //so that last markers in the loop overlay the earlier ones
            map: map,
            title: feature.city

          });

          // We are declaring the content attribute tot the marker so that we can reference it later with on click (this)
          marker.content = '<div class="scrollFix"><h2 class="mt0 mb0 fts18"><a href="events/' + feature.title_for_slug + '">' + feature.city + '</a></h2></div>'

          infowindow = new google.maps.InfoWindow();

          google.maps.event.addListener(marker, 'click', function() {
                if(!marker.open){
                    infowindow.setContent(this.content);
                    infowindow.open(map,marker);
                    marker.open = true;
                }
                else{
                    infowindow.close();
                    marker.open = false;
                }
                google.maps.event.addListener(map, 'click', function() {
                    infowindow.close();
                    marker.open = false;
                });
            });

        }

        // var features = [
        //   {
        //     position: new google.maps.LatLng(29.7274308,-95.4204557),
        //     content: '<h4 class="mt0 mb0">Houston</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(30.527504,-97.6331036),
        //     content: '<h4 class="mt0 mb0">Austin</h4>',
        //     type: 'lodging'
        //   }, {
        //     position: new google.maps.LatLng(28.5722847,-81.370535),
        //     content: '<h4 class="mt0 mb0">Orlando</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(39.2826741,-84.4709524),
        //     content: '<h4 class="mt0 mb0">Cincinnati</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(40.431478,-80.0505397),
        //     content: '<h4 class="mt0 mb0">Pittsburgh</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(33.7678358,-84.4906433),
        //     content: '<h4 class="mt0 mb0">Atlanta</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(26.1126653,-80.3134888),
        //     content: '<h4 class="mt0 mb0">South Florida</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(38.8635565,-77.3557175),
        //     content: '<h4 class="mt0 mb0">Washignton DC</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(40.0026767,-75.2581141),
        //     content: '<h4 class="mt0 mb0">Philadelphia</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(27.949605,-82.4638828),
        //     content: '<h4 class="mt0 mb0">Tampa Bay</h4>',
        //     type: 'shopping'
        //   }, {
        //     position: new google.maps.LatLng(40.7767833,-112.0605662),
        //     content: '<h4 class="mt0 mb0">Salt Lake City</h4>',
        //     type: 'shopping'
        //   }
        // ];

        var features = $('#mylocations').data('mylocations')

        for (var i = 0, feature; feature = features[i]; i++) {
          addMarker(feature);
        }

    }());

  }



























      
});





  //on turbolinks:before-cache destroy it owlCarousel // this is strictly so that when user presses browser back button, owl carousel still works normally
  $(document).on('turbolinks:before-cache', function() {
    $('.owl-carousel').owlCarousel('destroy');
  });
