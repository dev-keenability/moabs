class EventsController < ApplicationController
  require 'will_paginate/array'
  before_action :set_event, only: [:show]

  def index
    @myevents = Event.all
    upcoming_events = Event.where("event_date >= ?", Date.today).order(event_date: :asc)
    past_events = Event.where("event_date < ?", Date.today).order(event_date: :asc)
    events = upcoming_events.to_a << past_events.to_a
    events.flatten!
    @events = events.paginate(:page => params[:page], :per_page => 6)

    @eventindex = EventIndex.first
  end

  def show
    @partners = @event.partners
    @images = @event.images.order(position: :asc)
    
    event = Faq.where.not(answer: nil).first.event
    @default_faq = Faq.where.not(answer: nil).where(event: event)
  end


  private

    def set_event
      @event = Event.find_by(title_for_slug: params[:title_for_slug])
    end

end