class PagesController < ApplicationController
  def home
    @blogs = Category.includes(:blogs).last(3).collect { |c| c.blogs.last }
    @categories = Category.all.limit(3).order(position: :asc)
    @home_page = HomePage.first
    @events = Event.where("event_date >= ?", Date.today).order(event_date: :asc).limit(3)
    @images = @home_page.images.order(position: :asc)
  end

  def about_moabs
    @about_moabs = AboutMoab.first
    @images = @about_moabs.images.order(position: :asc)
  end

  def about_amy
    @about_amy = AboutAmy.first
    @images = @about_amy.images.order(position: :asc)
  end

  def partner_with_us
    @partner_with_us = PartnerWithU.first
    @images = @partner_with_us.images.order(position: :asc)
  end
end
