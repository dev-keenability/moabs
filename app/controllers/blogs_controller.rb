class BlogsController < ApplicationController
  before_action :set_blog, only: [:show]

  def index
    @blogs = Blog.order(pubdate: :desc).page(params[:page]).per_page(4)
    @categories = Category.order(name: :asc)
    @blog_index = BlogIndex.first
  end

  def index_category
    @blogs = Blog.where(category_id: Category.find_by(category_slug: params[:category_slug]).id).order(pubdate: :desc).page(params[:page]).per_page(4)
    @categories = Category.order(name: :asc)
    @blog_index = BlogIndex.first
    render :index
  end

  def show
    @related_blogs = Blog.where(category_id: @blog.category_id).where.not(id: @blog.id).last(3)
    @categories = Category.all
  end

  private

  def set_blog
    @blog = Blog.find_by(title_for_slug: params[:title_for_slug])
  end

end
