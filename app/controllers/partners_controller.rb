class PartnersController < ApplicationController
  before_action :set_partner, only: [:show]

  def index
    @partners = Partner.all
  end

  def show
    @images = @partner.images.order(position: :asc)
  end

  private

    def set_partner
      @partner = Partner.find_by(title_for_slug: params[:title_for_slug])
    end

end
