class PressController < ApplicationController
  before_action :set_press, only: [:show]

  def index
    @press = Press.order(pubdate: :desc).page(params[:page]).per_page(4)
    @press_index = PressIndex.first
  end

  def show
  end

  def press_page
    @press_page = PressPage.first
    @images = @press_page.images.order(position: :asc)
  end

  private

  def set_press
    @press = Press.find_by(title_for_slug: params[:title_for_slug])
  end

end
