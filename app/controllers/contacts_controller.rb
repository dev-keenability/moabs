require 'constantcontact'

class ContactsController < ApplicationController
  def create
    @contact = Contact.new(contact_params)
    
    if @contact.save && add_constant_contact
      # Tell the contactMailer to send a welcome email after save
      #ContactMailer.welcome_email(@contact).deliver_now
      #flash[:success] = 'Thank you for contacting us.'
      redirect_to root_path(anchor: 'maincontent')
    else
      flash[:notice] = "Email already exist"
      redirect_to root_path(anchor: 'maincontent')
    end
    
  end

  private

  def contact_params
    params.require(:contact).permit(:name, :email, :phone_number, :message, :zip)
  end
  
  def add_constant_contact
    cc = ConstantContact::Api.new(ENV['CC_API_KEY'], ENV['CC_TOKEN'])
    first_name = contact_params['name'].split(" ")[0]
    last_name = contact_params['name'].split(" ")[1]
    list_to_add_to = ConstantContact::Components::ContactList.new
    list_to_add_to.id = ENV['CC_LIST_ID']

    new_contact = ConstantContact::Components::Contact.new
    new_contact.add_email(ConstantContact::Components::EmailAddress.new(contact_params['email']))
    new_contact.add_list(list_to_add_to)
    new_contact.first_name = first_name.to_s
    new_contact.last_name = last_name.to_s
    address = "{\"postal_code\": \"#{contact_params['zip']}\", \"address_type\": \"PERSONAL\"}"
    new_contact.add_address(ConstantContact::Components::Address.create(JSON.parse(address)))
    categories = params['categories'].join(",") if params['categories']
    custom_fields = "{\"name\": \"custom_field_1\", \"value\": \"#{categories}\"}"
    new_contact.custom_fields = [ConstantContact::Components::CustomField.create(JSON.parse(custom_fields))]
    cc.add_contact(new_contact)
    true
  rescue RestClient::Conflict
    false
  end

end
