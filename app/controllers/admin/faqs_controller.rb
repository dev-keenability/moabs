class Admin::FaqsController < ApplicationController
  before_action :set_faq, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  def index
    if params[:event].present? && params[:event][:event_id].present?
      @faqs = Faq.where(event_id: params[:event][:event_id]).order(created_at: :asc)
    else
      @faqs = Faq.all.order(created_at: :asc)
    end
  end

  def new
    @faq = Faq.new
  end

  def create
    @faq = Faq.new(faq_params)
    if @faq.save
      redirect_to admin_faqs_path, notice: 'FAQ was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @faq.update(faq_params)
      redirect_to admin_faqs_path, notice: 'FAQ was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @faq.destroy
    redirect_to admin_faqs_path, notice: 'FAQ was successfully destroyed.'
  end

  private

    def set_faq
      @faq = Faq.find(params[:id])
    end

    def faq_params
      params.require(:faq).permit(:question, :answer, :event_id)
    end

  end