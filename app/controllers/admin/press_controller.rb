class Admin::PressController < ApplicationController
  before_action :set_press, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  def index
    @press = Press.order(pubdate: :desc)
  end

  def new
    @press = Press.new
  end

  def create
    @press = Press.new(press_params)
    if @press.save
      redirect_to admin_press_index_path, notice: 'press was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def edit_press_page
    @press_page = PressPage.first
  end

  def update_press_page
    @press_page = PressPage.first
    if @press_page.update(press_page_params)
      redirect_to admin_edit_press_page_path, notice: 'press was successfully updated.'
    else
      render :edit_press_page
    end
  end

  def update
    if @press.update(press_params)
      redirect_to admin_press_index_path, notice: 'press was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @press.destroy
    redirect_to admin_press_index_path, notice: 'press was successfully destroyed.'
  end

  def edit_images
    @press_index = PressIndex.first
  end

  def update_images
    @press_index = PressIndex.first
    if @press_index.update(press_index_params)
      redirect_to admin_press_index_images_path, notice: 'Press Index Images were successfully updated.'
    else
      render :edit_images
    end
  end





  def press_images
    @press_page = PressPage.first
    @press_images = @press_page.images.order(position: :asc)
    @press_image = @press_page.images.new
  end

  def press_image_create
    @press_page = PressPage.first
    @press_image = @press_page.images.new(press_image_params)
    @press_image.save
  end

  def press_image_delete
    @press_page = PressPage.first
    @press_image = @press_page.images.where(id: params[:img_id]).first
    @press_image.destroy
  end

  def press_image_sort
    data_sql_format = ""
      params[:press_image].each_with_index do | img_id, index |
        data_sql_format << "(#{img_id},#{index +1}),"
      end

      # This will do it in a single database update query O(1), however it requires O(n) calculations to format the data in a manner postgres likes it

      data_sql_format = data_sql_format[0...-1] # this is to remove the last comma, # i.e., data_sql_format = (1,1),(3,2),(2,3),(4,4) 

      ActiveRecord::Base.connection.execute(
        "update images as m set
            position = c.position
        from (values
            #{data_sql_format}
        ) as c(id, position) 
        where c.id = m.id
      ")

      # render nothing: true # deprecated in rails 5.1 use below
      head :ok
  end



  private

    def set_press
      @press = Press.find_by(title_for_slug: params[:title_for_slug])
    end

    def press_params
      params.require(:press).permit(:title, :content, :content_index, :title_for_slug, :main_image, :pubdate, :meta_description, :meta_keywords, :side_ad_link, :side_ad, :bottom_ad_one_link, :bottom_ad_one, :bottom_ad_two_link, :bottom_ad_two, :bottom_ad_three_link, :bottom_ad_three, images_attributes: [:id, :pic, :name, :primary, :_destroy])
    end

    def press_page_params
      # params.require(:press_page).permit(:main_image, :video_link, :description, :meta_keywords, :side_ad_link, :side_ad, :bottom_ad_one_link, :bottom_ad_one, :bottom_ad_two_link, :bottom_ad_two, :bottom_ad_three_link, :bottom_ad_three, images_attributes: [:id, :pic, :name, :primary, :_destroy])
      params.require(:press_page).permit(:main_image, :video_link, :description)
    end

    def press_image_params
      # params.require(:press_page).permit(:main_image, :video_link, :description, :meta_keywords, :side_ad_link, :side_ad, :bottom_ad_one_link, :bottom_ad_one, :bottom_ad_two_link, :bottom_ad_two, :bottom_ad_three_link, :bottom_ad_three, images_attributes: [:id, :pic, :name, :primary, :_destroy])
      params.require(:press_image).permit!
    end

    def press_index_params
      params.require(:press_index).permit(:bottom_ad_one, :bottom_ad_two, :bottom_ad_three, :bottom_ad_one_link, :bottom_ad_two_link, :bottom_ad_three_link, :side_ad, :side_ad_link)
    end

  end