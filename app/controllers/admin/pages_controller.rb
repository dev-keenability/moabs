class Admin::PagesController < ApplicationController
  before_action :authenticate_user!

  def admin
  end

  def home
    @home_page = HomePage.first
  end

  def home_update
    @home_page = HomePage.first
    if @home_page.update(home_page_params)
      redirect_to admin_pages_home_path, notice: 'About Moabs Page was successfully updated.'
    else
      render :home
    end
  end

  def about_moabs
    @about_moabs = AboutMoab.first
  end

  def about_moabs_update
    @about_moabs = AboutMoab.first
    if @about_moabs.update(about_moabs_params)
      redirect_to admin_pages_about_moabs_path, notice: 'About Moabs Page was successfully updated.'
    else
      render :about_moabs
    end
  end

  def about_amy
    @about_amy = AboutAmy.first
  end

  def about_amy_update
    @about_amy = AboutAmy.first
    if @about_amy.update(about_amy_params)
      redirect_to admin_pages_about_amy_path, notice: 'About Amy Page was successfully updated.'
    else
      render :about_amy
    end
  end

  def partner_with_us
    @partner_with_us = PartnerWithU.first
  end

  def partner_with_us_update
    @partner_with_us = PartnerWithU.first
    if @partner_with_us.update(partner_with_us_params)
      redirect_to admin_pages_partner_with_us_path, notice: 'About Amy Page was successfully updated.'
    else
      render :about_amy
    end
  end

  ##============================================================##
  ## Images
  ##============================================================##
  def images
    @home_page = HomePage.first
    @about_moabs = AboutMoab.first
    @about_amy = AboutAmy.first
    @partner_with_us = PartnerWithU.first
    @press_page = PressPage.first
  end

  def home_page_images
    @home_page = HomePage.first
    @home_page_images = @home_page.images.order(position: :asc)
    @home_page_image = @home_page.images.new
  end

  def home_page_image_create
    @home_page = HomePage.first
    @home_page_image = @home_page.images.new(home_page_image_params)
    @home_page_image.save
  end

  def home_page_image_delete
    @home_page = HomePage.first
    @home_page_image = @home_page.images.where(id: params[:img_id]).first
    @home_page_image.destroy
  end

  def home_page_image_sort
    data_sql_format = ""
      params[:home_page_image].each_with_index do | img_id, index |
        data_sql_format << "(#{img_id},#{index +1}),"
      end

      # This will do it in a single database update query O(1), however it requires O(n) calculations to format the data in a manner postgres likes it

      data_sql_format = data_sql_format[0...-1] # this is to remove the last comma, # i.e., data_sql_format = (1,1),(3,2),(2,3),(4,4) 

      ActiveRecord::Base.connection.execute(
        "update images as m set
            position = c.position
        from (values
            #{data_sql_format}
        ) as c(id, position) 
        where c.id = m.id
      ")

      # render nothing: true # deprecated in rails 5.1 use below
      head :ok
  end

  ##============================================================##
  ## About Amy
  ##============================================================##

  def about_amy_images
    @about_amy = AboutAmy.first
    @about_amy_images = @about_amy.images.order(position: :asc)
    @about_amy_image = @about_amy.images.new
  end

  def about_amy_image_create
    puts "#"*200
    puts params
    puts "#"*200
    @about_amy = AboutAmy.first
    @about_amy_image = @about_amy.images.new(about_amy_image_params)
    @about_amy_image.slider = "2" if params[:about_amy_asset][:slider].present?
    @about_amy_image.save
  end

  def about_amy_image_delete
    @about_amy = AboutAmy.first
    @about_amy_image = @about_amy.images.where(id: params[:img_id]).first
    @about_amy_image.destroy
  end

  def about_amy_image_sort
    data_sql_format = ""
      params[:about_amy_image].each_with_index do | img_id, index |
        data_sql_format << "(#{img_id},#{index +1}),"
      end

      # This will do it in a single database update query O(1), however it requires O(n) calculations to format the data in a manner postgres likes it

      data_sql_format = data_sql_format[0...-1] # this is to remove the last comma, # i.e., data_sql_format = (1,1),(3,2),(2,3),(4,4) 

      ActiveRecord::Base.connection.execute(
        "update images as m set
            position = c.position
        from (values
            #{data_sql_format}
        ) as c(id, position) 
        where c.id = m.id
      ")

      # render nothing: true # deprecated in rails 5.1 use below
      head :ok
  end

  ##============================================================##
  ## About Moab
  ##============================================================##

  def about_moab_images
    @about_moab = AboutMoab.first
    @about_moab_images = @about_moab.images.order(position: :asc)
    @about_moab_image = @about_moab.images.new
  end

  def about_moab_image_create
    @about_moab = AboutMoab.first
    @about_moab_image = @about_moab.images.new(about_moab_image_params)
    @about_moab_image.save
  end

  def about_moab_image_delete
    @about_moab = AboutMoab.first
    @about_moab_image = @about_moab.images.where(id: params[:img_id]).first
    @about_moab_image.destroy
  end

  def about_moab_image_sort
    data_sql_format = ""
      params[:about_moab_image].each_with_index do | img_id, index |
        data_sql_format << "(#{img_id},#{index +1}),"
      end

      # This will do it in a single database update query O(1), however it requires O(n) calculations to format the data in a manner postgres likes it

      data_sql_format = data_sql_format[0...-1] # this is to remove the last comma, # i.e., data_sql_format = (1,1),(3,2),(2,3),(4,4) 

      ActiveRecord::Base.connection.execute(
        "update images as m set
            position = c.position
        from (values
            #{data_sql_format}
        ) as c(id, position) 
        where c.id = m.id
      ")

      # render nothing: true # deprecated in rails 5.1 use below
      head :ok
  end

  ##============================================================##
  ## Partner With U
  ##============================================================##

  def partner_with_u_images
    @partner_with_u = PartnerWithU.first
    @partner_with_u_images = @partner_with_u.images.order(position: :asc)
    @partner_with_u_image = @partner_with_u.images.new
  end

  def partner_with_u_image_create
    @partner_with_u = PartnerWithU.first
    @partner_with_u_image = @partner_with_u.images.new(partner_with_u_image_params)
    @partner_with_u_image.save
  end

  def partner_with_u_image_delete
    @partner_with_u = PartnerWithU.first
    @partner_with_u_image = @partner_with_u.images.where(id: params[:img_id]).first
    @partner_with_u_image.destroy
  end

  def partner_with_u_image_sort
    data_sql_format = ""
      params[:partner_with_u_image].each_with_index do | img_id, index |
        data_sql_format << "(#{img_id},#{index +1}),"
      end

      # This will do it in a single database update query O(1), however it requires O(n) calculations to format the data in a manner postgres likes it

      data_sql_format = data_sql_format[0...-1] # this is to remove the last comma, # i.e., data_sql_format = (1,1),(3,2),(2,3),(4,4) 

      ActiveRecord::Base.connection.execute(
        "update images as m set
            position = c.position
        from (values
            #{data_sql_format}
        ) as c(id, position) 
        where c.id = m.id
      ")

      # render nothing: true # deprecated in rails 5.1 use below
      head :ok
  end




  ######################################################################################
  ## DELETE AD IMAGE
  ######################################################################################

  def delete_home_page_bottom_ad_one
    @home_page = HomePage.first
    @home_page.bottom_ad_one.destroy
    @home_page.bottom_ad_one = nil
    @home_page.save
    redirect_to admin_pages_home_path, notice: 'Ad was successfully deleted.'
  end

  def delete_home_page_bottom_ad_two
    @home_page = HomePage.first
    @home_page.bottom_ad_two.destroy
    @home_page.bottom_ad_two = nil
    @home_page.save
    redirect_to admin_pages_home_path, notice: 'Ad was successfully deleted.'
  end

  def delete_home_page_bottom_ad_three
    @home_page = HomePage.first
    @home_page.bottom_ad_three.destroy
    @home_page.bottom_ad_three = nil
    @home_page.save
    redirect_to admin_pages_home_path, notice: 'Ad was successfully deleted.'
  end



  def delete_about_amy_bottom_ad_one
    @about_amy = AboutAmy.first
    @about_amy.bottom_ad_one.destroy
    @about_amy.bottom_ad_one = nil
    @about_amy.save
    redirect_to admin_pages_about_amy_path, notice: 'Ad was successfully deleted.'
  end

  def delete_about_amy_bottom_ad_two
    @about_amy = AboutAmy.first
    @about_amy.bottom_ad_two.destroy
    @about_amy.bottom_ad_two = nil
    @about_amy.save
    redirect_to admin_pages_about_amy_path, notice: 'Ad was successfully deleted.'
  end

  def delete_about_amy_bottom_ad_three
    @about_amy = AboutAmy.first
    @about_amy.bottom_ad_three.destroy
    @about_amy.bottom_ad_three = nil
    @about_amy.save
    redirect_to admin_pages_about_amy_path, notice: 'Ad was successfully deleted.'
  end



  private

  def about_amy_params
    params.require(:about_amy).permit(:meet_amy, :background_img, :moabs_experience, :bottom_ad_one, :bottom_ad_one_link, :bottom_ad_two, :bottom_ad_two_link, :bottom_ad_three, :bottom_ad_three_link, :meta_title, :meta_description)
  end

  def about_moabs_params
    params.require(:about_moab).permit(:main_image, :video_link, :description, :meta_title, :meta_description)
  end

  def partner_with_us_params
    params.require(:partner_with_u).permit(:main_image, :description, :testimonial, :testimonial_author, :meta_title, :meta_description)
  end

  def home_page_params
    params.require(:home_page).permit(:main_image, :description, :video_link, :bottom_ad_one, :bottom_ad_one_link, :bottom_ad_two, :bottom_ad_two_link, :bottom_ad_three, :bottom_ad_three_link, :meta_title, :meta_description)
  end

  def home_page_image_params
    params.require(:home_page_image).permit!
  end

  def about_amy_image_params
    params.require(:about_amy_image).permit!
  end

  def about_moab_image_params
    params.require(:about_moab_image).permit!
  end

  def partner_with_u_image_params
    params.require(:partner_with_u_image).permit!
  end

end


