class Admin::ImagesController < ApplicationController
  before_action :set_image, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  def index
    # @images = Image.all
    # if params[:event].present? && params[:event][:event_id].present?
    #   @faqs = Faq.where(event_id: params[:event][:event_id])
    # else
    #   @faqs = Faq.all
    # end
    if params[:event].present?
      @mymodel = Event.find(params[:event].to_i)
    elsif params[:blog].present?
      @mymodel = Blog.find(params[:blog].to_i)
    end
  end

  def new
    @image = Image.new
  end

  def create
    @image = Image.new(image_params)
    if @image.save
      redirect_to admin_images_path, notice: 'image was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @image.update(image_params)
      redirect_to admin_images_path, notice: 'image was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @image.destroy
    redirect_to admin_images_path, notice: 'image was successfully destroyed.'
  end

  private

    def set_image
      @image = Image.find(params[:id])
    end

    def image_params
      params.require(:image).permit(:pic, :slider)
    end

  end