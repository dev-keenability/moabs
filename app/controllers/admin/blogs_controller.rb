class Admin::BlogsController < ApplicationController
  before_action :set_blog, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :authenticate_user!


  def index
    @blogs = Blog.order(pubdate: :desc)
  end

  def new
    @blog = Blog.new
  end

  def create
    @blog = Blog.new(blog_params)
    if @blog.save
      redirect_to admin_blogs_path, notice: 'Blog was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @blog.update(blog_params)
      redirect_to admin_blogs_path, notice: 'Blog was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @blog.destroy
    redirect_to admin_blogs_path, notice: 'Blog was successfully destroyed.'
  end



  def edit_images
    @blog_index = BlogIndex.first
  end

  def update_images
    @blog_index = BlogIndex.first
    if @blog_index.update(blog_index_params)
      redirect_to admin_blog_index_images_path, notice: 'Blog Index Images were successfully updated.'
    else
      render :edit_images
    end
  end

  def delete_blogs_bottom_ad_one
    @blog = Blog.find_by(title_for_slug: params[:title_for_slug])
    @blog.bottom_ad_one.destroy
    @blog.bottom_ad_one = nil
    @blog.save
    redirect_to admin_edit_blog_path(@blog.title_for_slug), notice: 'Ad was successfully deleted.'
  end

  def delete_blogs_bottom_ad_two
    @blog = Blog.find_by(title_for_slug: params[:title_for_slug])
    @blog.bottom_ad_two.destroy
    @blog.bottom_ad_two = nil
    @blog.save
    redirect_to admin_edit_blog_path(@blog.title_for_slug), notice: 'Ad was successfully deleted.'
  end

  def delete_blogs_bottom_ad_three
    @blog = Blog.find_by(title_for_slug: params[:title_for_slug])
    @blog.bottom_ad_three.destroy
    @blog.bottom_ad_three = nil
    @blog.save
    redirect_to admin_edit_blog_path(@blog.title_for_slug), notice: 'Ad was successfully deleted.'
  end

  def delete_blogs_side_ad
    @blog = Blog.find_by(title_for_slug: params[:title_for_slug])
    @blog.side_ad.destroy
    @blog.side_ad = nil
    @blog.save
    redirect_to admin_edit_blog_path(@blog.title_for_slug), notice: 'Ad was successfully deleted.'
  end



  def delete_blogs_index_bottom_ad_one
    @blog_index = BlogIndex.first
    @blog_index.bottom_ad_one.destroy
    @blog_index.bottom_ad_one = nil
    @blog_index.save
    redirect_to admin_blog_index_images_path, notice: 'Ad was successfully deleted.'
  end

  def delete_blogs_index_bottom_ad_two
    @blog_index = BlogIndex.first
    @blog_index.bottom_ad_two.destroy
    @blog_index.bottom_ad_two = nil
    @blog_index.save
    redirect_to admin_blog_index_images_path, notice: 'Ad was successfully deleted.'
  end

  def delete_blogs_index_bottom_ad_three
    @blog_index = BlogIndex.first
    @blog_index.bottom_ad_three.destroy
    @blog_index.bottom_ad_three = nil
    @blog_index.save
    redirect_to admin_blog_index_images_path, notice: 'Ad was successfully deleted.'
  end

  def delete_blogs_index_side_ad
    @blog_index = BlogIndex.first
    @blog_index.side_ad.destroy
    @blog_index.side_ad = nil
    @blog_index.save
    redirect_to admin_blog_index_images_path, notice: 'Ad was successfully deleted.'
  end


   # match 'blogs/:title_for_slug/delete_ad_three_image'               => 'blogs#delete_ad_three_image',        :via => [:delete],        :as => :delete_ad_three_image
   #    match 'blogs/:title_for_slug/delete_side_ad_one_image'               => 'blogs#delete_side_ad_one_image',        :via => [:delete],        :as => :delete_side_ad_one_image


  private

    def set_blog
      @blog = Blog.find_by(title_for_slug: params[:title_for_slug])
    end

    def blog_params
      params.require(:blog).permit(:title, :content, :content_index, :category_id, :title_for_slug, :main_image, :pubdate, :side_ad_link, :side_ad, :bottom_ad_one_link, :bottom_ad_one, :bottom_ad_two_link, :bottom_ad_two, :bottom_ad_three_link, :bottom_ad_three, :meta_description, :meta_keywords, images_attributes: [:id, :pic, :name, :primary, :_destroy])
    end

    def blog_index_params
      params.require(:blog_index).permit(:bottom_ad_one, :bottom_ad_two, :bottom_ad_three, :bottom_ad_one_link, :bottom_ad_two_link, :bottom_ad_three_link, :side_ad, :side_ad_link)
    end

  end