class Admin::CategoriesController < ApplicationController
  before_action :set_category, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  def index
    @categories = Category.all.order(position: :asc)
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new(category_params)
    if @category.save
      redirect_to admin_categories_path, notice: 'Category was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    # if @category.update(category_params)
    #   redirect_to admin_categories_path, notice: 'Category was successfully updated.'
    # else
    #   render :edit
    # end

    if !params[:category][:image].present?
      if @category.update(category_without_image_params)
        redirect_to admin_categories_path, notice: 'Category was successfully updated.'
      else
        render :edit
      end
    else
      @category.image.destroy
      if @category.update(category_params)
        redirect_to admin_categories_path, notice: 'Category was successfully updated.'
      else
        render :edit
      end
    end
  end

  def destroy
    @category.destroy
    redirect_to admin_categories_path, notice: 'Category was successfully destroyed.'
  end

  private

    def set_category
      @category = Category.find_by(category_slug: params[:category_slug])
    end

    def category_params
      params.require(:category).permit(:name, :category_slug, :image, :position)
    end

    def category_without_image_params
      params.require(:category).permit(:name, :category_slug, :position)
    end

  end