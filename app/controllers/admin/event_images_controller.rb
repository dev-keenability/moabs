class Admin::EventImagesController < ApplicationController
  before_action :set_event, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  def index
    @event_images = EventImage.all
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      redirect_to admin_events_path, notice: 'event was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @event.update(event_params)
      redirect_to admin_events_path, notice: 'event was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @event.destroy
    redirect_to admin_events_path, notice: 'event was successfully destroyed.'
  end

  private

    def set_event
      @event = Event.find_by(title_for_slug: params[:title_for_slug])
    end

    def event_params
      params.require(:event).permit(:city, :main_image, :event_description, :event_date, :address, :latitude, :longitude, :agenda_title, :agenda_description, :eventbrite_link, :side_ad_one_image, :side_ad_one_link, :ad_one_image, :ad_one_link, :ad_two_image, :ad_two_link, :ad_three_image, :ad_three_link, :title_for_slug, :meta_title, :meta_description, :location_description)
    end

  end