class Admin::PartnersController < ApplicationController
  before_action :set_partner, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  def index
    @partners = Partner.all
  end

  def new
    @partner = Partner.new
  end

  def create
    @partner = Partner.new(partner_params)
    if @partner.save
      redirect_to admin_partners_path, notice: 'Partner was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    # if @partner.update(category_params)
    #   redirect_to admin_partners_path, notice: 'Category was successfully updated.'
    # else
    #   render :edit
    # end

    if !params[:partner][:main_image].present?
      if @partner.update(partner_without_image_params)
        redirect_to admin_partners_path, notice: 'Partner was successfully updated.'
      else
        render :edit
      end
    else
      @partner.main_image.destroy
      if @partner.update(partner_params)
        redirect_to admin_partners_path, notice: 'Partner was successfully updated.'
      else
        render :edit
      end
    end
  end

  def destroy
    @partner.destroy
    redirect_to admin_partners_path, notice: 'Partner was successfully destroyed.'
  end


  ##============================================================##
  ## Images
  ##============================================================##
  def images
    @partners = Partner.all
  end

  def partner_image
    @partner = Partner.find_by(title_for_slug: params[:title_for_slug])
    @partner_images = @partner.images.order(position: :asc)
    @partner_image = @partner.images.new
  end

  def partner_image_create
    @partner = Partner.find_by(title_for_slug: params[:title_for_slug])
    @partner_image = @partner.images.new(partner_image_params)
    @partner_image.save
  end

  def partner_image_delete
    @partner = Partner.find_by(title_for_slug: params[:title_for_slug])
    @partner_image = @partner.images.where(id: params[:img_id]).first
    @partner_image.destroy
  end

  def partner_image_sort
    data_sql_format = ""
      params[:partner_image].each_with_index do | img_id, index |
        data_sql_format << "(#{img_id},#{index +1}),"
      end

      # This will do it in a single database update query O(1), however it requires O(n) calculations to format the data in a manner postgres likes it

      data_sql_format = data_sql_format[0...-1] # this is to remove the last comma, # i.e., data_sql_format = (1,1),(3,2),(2,3),(4,4) 

      ActiveRecord::Base.connection.execute(
        "update images as m set
            position = c.position
        from (values
            #{data_sql_format}
        ) as c(id, position) 
        where c.id = m.id
      ")

      # render nothing: true # deprecated in rails 5.1 use below
      head :ok
  end

  private

    def set_partner
      @partner = Partner.find_by(title_for_slug: params[:title_for_slug])
    end

    def partner_params
      params.require(:partner).permit(:name, :title_for_slug, :main_image, :weblink, :description, :meta_description, :testimonial, :testimonial_author)
    end

    def partner_without_image_params
      params.require(:partner).permit(:name, :title_for_slug, :weblink, :description, :meta_description, :testimonial, :testimonial_author)
    end

    def partner_image_params
      params.require(:partner_image).permit!
    end
end