class Admin::EventsController < ApplicationController
  before_action :set_event, only: [:edit, :update, :destroy]
  # before_action :authenticate_user!, only: [:new, :create, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  def index
    @events = Event.all
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      redirect_to admin_events_path, notice: 'event was successfully created.'
    else
      render :new
    end
  end

  def edit
  end

  def update
    if @event.update(event_params)
      redirect_to admin_events_path, notice: 'event was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @event.destroy
    redirect_to admin_events_path, notice: 'event was successfully destroyed.'
  end



  def edit_images
    @eventindex = EventIndex.first
  end

  def update_images
    @eventindex = EventIndex.first
    if @eventindex.update(event_index_params)
      redirect_to admin_event_index_images_path, notice: 'Event Index Images was successfully updated.'
    else
      render :edit_images
    end
  end

  def create_images
    @eventindex = EventIndex.new(event_index_params)
    if @eventindex.save
      redirect_to admin_event_index_images_path, notice: 'Event Index Images was successfully created.'
    else
      render :new
    end
  end



  ##============================================================##
  ## Images
  ##============================================================##
  def images
    @events = Event.all.order(event_date: :asc)
  end

  def event_image
    @event = Event.find_by(title_for_slug: params[:title_for_slug])
    @event_images = @event.images.order(position: :asc)
    @event_image = @event.images.new
  end

  def event_image_create
    @event = Event.find_by(title_for_slug: params[:title_for_slug])
    @event_image = @event.images.new(event_image_params)
    @event_image.save
  end

  def event_image_delete
    @event = Event.find_by(title_for_slug: params[:title_for_slug])
    @event_image = @event.images.where(id: params[:img_id]).first
    @event_image.destroy
  end

  def event_image_sort
    data_sql_format = ""
      params[:event_image].each_with_index do | img_id, index |
        data_sql_format << "(#{img_id},#{index +1}),"
      end

      # This will do it in a single database update query O(1), however it requires O(n) calculations to format the data in a manner postgres likes it

      data_sql_format = data_sql_format[0...-1] # this is to remove the last comma, # i.e., data_sql_format = (1,1),(3,2),(2,3),(4,4) 

      ActiveRecord::Base.connection.execute(
        "update images as m set
            position = c.position
        from (values
            #{data_sql_format}
        ) as c(id, position) 
        where c.id = m.id
      ")

      # render nothing: true # deprecated in rails 5.1 use below
      head :ok
  end



  def delete_ad_one_image
    @event = Event.find_by(title_for_slug: params[:title_for_slug])
    @event.ad_one_image.destroy
    @event.ad_one_image = nil
    @event.save
    redirect_to admin_edit_event_path(@event.title_for_slug), notice: 'Ad was successfully deleted.'
  end

  def delete_ad_two_image
    @event = Event.find_by(title_for_slug: params[:title_for_slug])
    @event.ad_two_image.destroy
    @event.ad_two_image = nil
    @event.save
    redirect_to admin_edit_event_path(@event.title_for_slug), notice: 'Ad was successfully deleted.'
  end

  def delete_ad_three_image
    @event = Event.find_by(title_for_slug: params[:title_for_slug])
    @event.ad_three_image.destroy
    @event.ad_three_image = nil
    @event.save
    redirect_to admin_edit_event_path(@event.title_for_slug), notice: 'Ad was successfully deleted.'
  end

  def delete_side_ad_one_image
    @event = Event.find_by(title_for_slug: params[:title_for_slug])
    @event.side_ad_one_image.destroy
    @event.side_ad_one_image = nil
    @event.save
    redirect_to admin_edit_event_path(@event.title_for_slug), notice: 'Ad was successfully deleted.'
  end




  def delete_event_index_bottom_ad_one
    @eventindex = EventIndex.first
    @eventindex.bottom_ad_one.destroy
    @eventindex.bottom_ad_one = nil
    @eventindex.save
    redirect_to admin_event_index_images_path, notice: 'Ad was successfully deleted.'
  end

  def delete_event_index_bottom_ad_two
    @eventindex = EventIndex.first
    @eventindex.bottom_ad_two.destroy
    @eventindex.bottom_ad_two = nil
    @eventindex.save
    redirect_to admin_event_index_images_path, notice: 'Ad was successfully deleted.'
  end

  def delete_event_index_bottom_ad_three
    @eventindex = EventIndex.first
    @eventindex.bottom_ad_three.destroy
    @eventindex.bottom_ad_three = nil
    @eventindex.save
    redirect_to admin_event_index_images_path, notice: 'Ad was successfully deleted.'
  end

  def delete_event_index_side_ad_one
    @eventindex = EventIndex.first
    @eventindex.side_ad_one.destroy
    @eventindex.side_ad_one = nil
    @eventindex.save
    redirect_to admin_event_index_images_path, notice: 'Ad was successfully deleted.'
  end

  def delete_event_index_side_ad_two
    @eventindex = EventIndex.first
    @eventindex.side_ad_two.destroy
    @eventindex.side_ad_two = nil
    @eventindex.save
    redirect_to admin_event_index_images_path, notice: 'Ad was successfully deleted.'
  end


  private

    def set_event
      @event = Event.find_by(title_for_slug: params[:title_for_slug])
    end

    def event_params
      params.require(:event).permit(:city, :main_image, :cover_image, :icon, :event_description, :video_link, :event_date, :address, :latitude, :longitude, :agenda_title, :agenda_description, :eventbrite_link, :side_ad_one_image, :side_ad_one_link, :ad_one_image, :ad_one_link, :ad_two_image, :ad_two_link, :ad_three_image, :ad_three_link, :title_for_slug, :meta_title, :meta_description, :location_description, :partner_ids => [], images_attributes: [:id, :pic, :name, :primary, :position, :_destroy])
    end

    def event_index_params
      params.require(:event_index).permit(:bottom_ad_one, :bottom_ad_two, :bottom_ad_three, :side_ad_one, :side_ad_two, :bottom_ad_one_link, :bottom_ad_two_link, :bottom_ad_three_link, :side_ad_one_link, :side_ad_two_link)
    end

    def event_image_params
      params.require(:event_image).permit!
    end

  end