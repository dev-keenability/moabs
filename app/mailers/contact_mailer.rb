class ContactMailer < ApplicationMailer
  default from: ENV["GMAIL_USERNAME"]

  def welcome_email(contact)
    @contact = contact
    # mail(to: @contact.email, subject: 'You have received a new contact from moabs website')
    mail(to: ENV["GMAIL_EMAIL"], subject: 'You have received a new contact from moabs website')
  end
end
